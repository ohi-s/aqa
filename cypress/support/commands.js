Cypress.Commands.add('getQa', (value) => {
    return cy.get(`[data-test=${value}]`)
})

Cypress.Commands.add('login', (user) => {
    cy.session([user.email, user.password], () => {
        cy.visit('/login');
        cy.wait(8000);
        cy.getQa('email-field').type(user.email);
        cy.getQa('password-field').type(user.password);
        cy.getQa('signIn-btn').click();
        cy.getQa('avatar-btn').should('be.visible');
    })
})

Cypress.Commands.add('getStripeElement', (selector, value) => {
    cy.get('iframe')
        .should((iframe) => expect(iframe.contents().find(selector)).to.exist)
        .then((iframe) => cy.wrap(iframe.contents().find(selector)))
        .within((input) => {
            cy.wrap(input).should('not.be.disabled').clear().type(value)
        })
})

Cypress.Commands.add('clearCart', (user) => {
    cy.visit('https://release.ohi-s.com/cart/')
    cy.get('body').then((body) => {
        if (body.find('.trash-button').length == 0) {
            cy.log('Корзина пуста');
        }
        else {
            cy.get('.cart-wrapper__body').within(() => {
                cy.get('.trash-button', {timeout: 5000}).click({ multiple: true});
            })
            cy.get('.banner__title').should('contain', 'No products in the cart');
        }
    })
})

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

Cypress.Commands.add('getStripeElement', (selector, value) => {
    cy.get('iframe')
        .should((iframe) => expect(iframe.contents().find(selector)).to.exist)
        .then((iframe) => cy.wrap(iframe.contents().find(selector)))
        .within((input) => {
            cy.wrap(input).should('not.be.disabled').clear().type(value)
        })
})

Cypress.Commands.add('solveGoogleReCAPTCHA', () => {
    // Wait until the iframe (Google reCAPTCHA) is totally loaded
    cy.wait(500);
    cy.get('#g-recaptcha *> iframe')
        .then($iframe => {
            const $body = $iframe.contents().find('body');
            cy.wrap($body)
                .find('.recaptcha-checkbox-border')
                .should('be.visible')
                .click();
        });
});
