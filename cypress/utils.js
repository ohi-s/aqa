export function randomString(length = 10) {
    var email = ""
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)

    text += possible.charAt(Math.floor(Math.random() * possible.length));
    var value = email + text.concat('@test.cy')
    return value;
}

export function changeCurrencyEUR() {
    cy.get('.o-switching-language-currency').realHover('mouse');
    cy.getQa('EUR-menu').click({force: true});
}

export function changeCurrencyUSD() {
    cy.get('.o-switching-language-currency').realHover('mouse');
    cy.getQa('USD-menu').click({force: true});
}

export function changeCurrencyRUB() {
    cy.get('.o-switching-language-currency').realHover('mouse');
    cy.getQa('RUB-menu').click({force: true});
}

export function changeLanguageRU() {
    cy.getQa('lead-form-base').should('be.visible');
    cy.get('.o-switching-language-currency__icon').realHover('mouse');
    cy.getQa('Русский-menu').click();
}

export function changeLanguageEng() {
    cy.getQa('lead-form-base').should('be.visible');
    cy.get('.o-switching-language-currency__icon').realHover('mouse');
    cy.getQa('English-menu').click();
}

export function changeLanguageEs() {
    cy.getQa('lead-form-base').should('be.visible');
    cy.get('.o-switching-language-currency__icon').realHover('mouse');
    cy.getQa('Español-menu').click();
}

export function changeLanguageDe() {
    cy.getQa('lead-form-base').should('be.visible');
    cy.get('.o-switching-language-currency__icon').realHover('mouse');
    cy.getQa('Deutsch-menu').click();
}

export function changeLanguageIt() {
    cy.getQa('lead-form-base').should('be.visible');
    cy.get('.o-switching-language-currency__icon').realHover('mouse');
    cy.getQa('Italiano-menu').click();
}

export function changeLanguagePt() {
    cy.getQa('lead-form-base').should('be.visible');
    cy.get('.o-switching-language-currency__icon').realHover('mouse');
    cy.getQa('Português-menu').click();
}

export function changeLanguageUkr() {
    cy.getQa('lead-form-base').should('be.visible');
    cy.get('.o-switching-language-currency__icon').realHover('mouse');
    cy.getQa('Українська-menu').click();
}

export function registrationUser() {
    cy.visit('/registration/')
    cy.wait(1000);
    cy.get('.el-form-item__content ').eq(0).type('Test');
    cy.get('.el-form-item__content ').eq(1).type('Testov');
    cy.get('.el-form-item__content ').eq(2).clear().click().type('+375333123123');
    cy.getQa('email-field').type(randomString());
    cy.get('.el-form-item__content ').eq(4).type('12345678');
    cy.getQa('signup-btn').click();
    cy.wait(5000);
}

export function paymentStripe() {
    cy.getStripeElement(
        'input[data-elements-stable-field-name="cardNumber"]',
        '4242424242424242'
    )
    cy.getStripeElement(
        'input[data-elements-stable-field-name="cardExpiry"]',
        '1026'
    )
    cy.getStripeElement('input[data-elements-stable-field-name="cardCvc"]', '123')
}




