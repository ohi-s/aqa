describe('Registered user', { tags: ['visual', 'web'] }, () => {

    context('Subscription is active', () => {
        beforeEach(() => {
            cy.login({ email: 'sergey.kovalyk@yandex.ru', password: '12345678' })
        })

        it('Registered: Main page', {tags: "@visual"}, () => {
            cy.visit('/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Main page');
        })

        it('Registered: Catalog', {tags: "@visual"}, () => {
            cy.visit('/learning/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Catalog');
        })

        it('Registered: Courses PLP', {tags: "@visual"}, () => {
            cy.visit('/courses/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Courses PLP');
        })

        it('Registered: Seminar PLP', {tags: "@visual"}, () => {
            cy.visit('/seminars/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Seminar PLP');
        })

        it('Registered: Seminar PDP', {tags: "@visual"}, () => {
            cy.visit('/seminars/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Seminar PDP');
        })

        it ('Registered: Сourses PDP', { tags: "@visual"}, () =>{
            cy.visit('/webinars-courses/157525/');
            cy.wait(5000)
            cy.percySnapshot('Registered: Сourses PDP');
        })

        it ('Registered: Webinar PDP', { tags: "@visual"}, () =>{
            cy.visit('/webinars/12588/');
            cy.wait(5000)
            cy.percySnapshot('Registered: Webinar PDP');
        })

        it('Registered: Chat', {tags: "@visual"}, () => {
            cy.visit('/chat/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Chat');
        })

        it('Registered: Subscription', {tags: "@visual"}, () => {
            cy.visit('/my-subscription/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Subscription');
        })

        it('Registered: Finance', {tags: "@visual"}, () => {
            cy.visit('/finance/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Finance');
        })

        it('Registered: Payments', {tags: "@visual"}, () => {
            cy.visit('/payments/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Payments');
        })

        it('Registered: Accesses', {tags: "@visual"}, () => {
            cy.visit('/accesses/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Accesses');
        })

        it('Registered: Settings main', {tags: "@visual"}, () => {
            cy.visit('/settings/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Settings main');
        })

        it('Registered: Settings sertificate', {tags: "@visual"}, () => {
            cy.visit('/settings/certificate/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Settings sertificate');
        })

        it('Registered: Settings general', {tags: "@visual"}, () => {
            cy.visit('/settings/general/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Settings general');
        })

        it('Registered: Settings managment', {tags: "@visual"}, () => {
            cy.visit('/settings/management/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Settings managment');
        })

        it('Registered: Settings security', {tags: "@visual"}, () => {
            cy.visit('/settings/security/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Settings security');
        })

        it('Registered: Empty Cart', {tags: "@visual"}, () => {
            cy.visit('/cart/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Empty Cart');
        })

        // нет data атрибута
        // it('Registered: Cart with product', {tags: "@visual"}, () => {
        //     cy.visit('/webinars/168315/')
        //
        //     cy.getQa('right-bar_add-to-cart',{timeout: 5000}).click();
        //     cy.visit('/cart/')
        //     cy.wait(5000)
        //     cy.percySnapshot('Registered: Cart with product');
        //     cy.clearCart()
        // })
    })

    context('Subscription (not active)', () => {
        it('Registered: Subscription (not active)', {tags: "@visual"}, () => {
            cy.visit('/subscription/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Subscription (not active)');
        })
    })

    context('Social', () => {
        beforeEach(() => {
            cy.login({ email: 'sergey.kovalyk@yandex.ru', password: '12345678' })
        })

        it ('Registered: People', { tags: "@visual"}, () =>{
            cy.visit('/social/people/?page=1&per_page=12&updated_at=desc')
            cy.wait(5000)
            cy.percySnapshot('Registered: People');
        })

        it ('Registered: Lecturers', { tags: "@visual"}, () =>{
            cy.visit('/social/people/?page=1&per_page=12&updated_at=desc&is_lecturer=true')
            cy.wait(5000)
            cy.percySnapshot('Registered: Lecturers');
        })

        it ('Registered: My contacts', { tags: "@visual"}, () =>{
            cy.visit('/social/my-contacts/')
            cy.wait(5000)
            cy.percySnapshot('Registered: My contacts');
        })

        // it ('Registered: My contacts (People for whom I am a contact)', { tags: "@visual"}, () =>{
        //     cy.visit('/social/my-contacts/');
        //     cy.get('#tab-forWhomIAmContact').click();
        //     cy.wait(8000)
        //     cy.percySnapshot('Registered: My contacts (People for whom I am a contact)');
        // })
        //
        // it ('Registered: My contacts (Invitations)', { tags: "@visual"}, () =>{
        //     cy.visit('/social/my-contacts/');
        //     cy.get('.#tab-invitations').click();
        //     cy.wait(8000)
        //     cy.percySnapshot('Registered: My contacts (Invitations)');
        // })

        // it ('Registered: My contacts (Invitations - Sent)', { tags: "@visual"}, () =>{
        //     cy.visit('/social/my-contacts/');
        //     cy.get('.#tab-invitations').eq(0).click();
        //     cy.get('.#tab-sent').click();
        //     cy.wait(5000)
        //     cy.percySnapshot('Registered: My contacts (Invitations - Sent)');
        // })

        it ('Registered: Lecturers profile', { tags: "@visual"}, () =>{
            cy.visit('/social/profile/10115/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Lecturers profile');
        })

        it ('Registered: Social profile', { tags: "@visual"}, () =>{
            cy.visit('/social/profile/13337/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Social profile');
        })

        it ('Registered: Social profile (my contact)', { tags: "@visual"}, () =>{
            cy.visit('/social/profile/13337/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Social profile (my contact)');
        })

        it ('Registered: Social profile (for whom I am a contact)', { tags: "@visual"}, () =>{
            cy.visit('/social/profile/13337/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Social profile (for whom I am a contact)');
        })

    })
})
