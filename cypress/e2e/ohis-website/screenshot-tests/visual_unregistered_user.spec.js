describe('Unregistered user', { tags: ['visual', 'web'] }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.clearCart()
    })

    context('Education', { tags: "@visual"}, () => {
        it ('Unregistered: Main page (ENG)', {tags: "@visual"}, () => {
            cy.visit('/')
            cy.wait(5000)
            cy.percySnapshot('Main page (ENG)');
        })

        it('Unregistered: Main page (RUS)', {tags: "@visual"}, () => {
            cy.visit('https://ru.release.ohi-s.com/')
            cy.wait(5000)
            cy.percySnapshot('Main page (RUS)');
        })

        it('Unregistered: Main page (ES)', {tags: "@visual"}, () => {
            cy.visit('https://es.release.ohi-s.com/')
            cy.wait(5000)
            cy.percySnapshot('Main page (ES)');
        })

        it('Unregistered: Main page (DE)', {tags: "@visual"}, () => {
            cy.visit('https://de.release.ohi-s.com/')
            cy.wait(5000)
            cy.percySnapshot('Main page (DE)');
        })

        it('Unregistered: Main page (IT)', {tags: "@visual"}, () => {
            cy.visit('https://it.release.ohi-s.com/')
            cy.wait(5000)
            cy.percySnapshot('Main page (IT)');
        })

        it('Unregistered: Main page (PT)', {tags: "@visual"}, () => {
            cy.visit('https://pt.release.ohi-s.com/')
            cy.wait(5000)
            cy.percySnapshot('Main page (PT)');
        })

        it('Unregistered: Login', {tags: "@visual"}, () => {
            cy.visit('/login/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Login');
        })

        it('Unregistered: Registration', {tags: "@visual"}, () => {
            cy.visit('/registration/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Registration');
        })

        it ('Unregistered: Reset password', {tags: "@visual"}, () => {
            cy.visit('/reset-password/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Reset password');
        })

        it ('Unregistered: Restore', {tags: "@visual"}, () => {
            cy.visit('/restore/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Restore');
        })

        it ('Unregistered: Verify email', {tags: "@visual"}, () => {
            cy.visit('/verify-email/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Verify email');
        })

        it ('Unregistered: Chat', {tags: "@visual"}, () => {
            cy.visit('/chat/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Chat');
        })

        it ('Unregistered: Learning', {tags: "@visual"}, () => {
            cy.visit('/learning/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Learning');
        })

        it ('Unregistered: Сourses PLP', {tags: "@visual"}, () => {
            cy.visit('/courses/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Сourses PLP');
        })

        it ('Unregistered: Seminars PLP', {tags: "@visual"}, () => {
            cy.visit('/seminars/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Seminars PLP');
        })

        it ('Unregistered: Seminar PDP', { tags: "@visual"}, () =>{
            cy.visit('/seminar/645/');
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP офлайна');
        })

        it ('Unregistered: Сourses PDP', { tags: "@visual"}, () =>{
            cy.visit('/webinars-courses/157525/');
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP урока онлайн');
        })

        it ('Unregistered: Webinar PDP', { tags: "@visual"}, () =>{
            cy.visit('/webinars/12588/');
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP урока онлайн');
        })

        it ('Unregistered: Organizations page', {tags: "@visual"}, () => {
            cy.visit('/companies/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Organizations page');
        })

        it ('Unregistered: Organization (Information)', {tags: "@visual"}, () => {
            cy.visit('/company/OHI-S/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Organization (Information)');
        })

        it ('Unregistered: Organization (courses)', {tags: "@visual"}, () => {
            cy.visit('/company/OHI-S/learning/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Organization (learning)');
        })

        it ('Unregistered: Organization (seminars)', {tags: "@visual"}, () => {
            cy.visit('/company/OHI-S/market/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Organization (market)');
        })

        it ('Unregistered: Contacts', {tags: "@visual"}, () =>{
            cy.visit('/contacts/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Contacts');
        })

        it ('Unregistered: Payment Methods', {tags: "@visual"}, () =>{
            cy.visit('/payment-methods/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Payment Methods');
        })

        it ('Unregistered: Offer', {tags: "@visual"}, () =>{
            cy.visit('/offer/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Offer');
        })

        it ('Unregistered: Confidentiality', { tags: "@visual"}, () =>{
            cy.visit('/confidentiality/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Confidentiality');
        })

        it ('Unregistered: Cookie Policy', { tags: "@visual"}, () =>{
            cy.visit('/cookie-policy/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Cookie Policy');
        })

        it ('Unregistered: Refund policy', { tags: "@visual"}, () =>{
            cy.visit('/refund-policy/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Refund policy');
        })

        it ('Unregistered: Empty Cart', { tags: "@visual"}, () =>{
            cy.visit('/cart/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Empty Cart');
        })

        // нет data атрибута в проекте
        // it ('Unregistered: Cart with product', { tags: "@visual"}, () =>{
        //     cy.visit('/seminar/168970/');
        //     cy.getQa('right-bar_add-to-cart').within(() => {
        //         cy.get('.el-button').eq(2).click()
        //     })
        //     cy.visit('/cart/')
        //     cy.wait(5000)
        //     cy.percySnapshot('Registered: Cart with product');
        // })

        it ('Unregistered: Wishlist', { tags: "@visual"}, () =>{
            cy.visit('https://mobile.release.ohi-s.com/wishlist/')
            cy.wait(5000)
            cy.percySnapshot('Registered: Wishlist');
        })

        it ('Unregistered: Cart error', { tags: "@visual"}, () =>{
            cy.visit('/cart/error/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Cart error');
        })

        it ('Unregistered: Cart success', { tags: "@visual"}, () =>{
            cy.visit('/cart/success/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Cart success');
        })

        it ('Unregistered: Search', { tags: "@visual"}, () =>{
            cy.visit('/search/online/?search=web');

            cy.wait(5000)
            cy.percySnapshot('Unregistered: Search');
        })

        it ('Unregistered: Search lecturers', { tags: "@visual"}, () =>{
            cy.visit('/search/lecturers/?search=slav&page=1');

            cy.wait(5000)
            cy.percySnapshot('Unregistered: Search lecturers');
        })

        it ('Unregistered: Search seminars', { tags: "@visual"}, () =>{
            cy.visit('/search/seminars/?search=web&page=1');

            cy.wait(5000)
            cy.percySnapshot('Unregistered: Search seminars');
        })

        it ('Unregistered: Subscription', {tags: "@visual"}, () => {
            cy.visit('/subscription/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Subscription');
        })

        it ('Unregistered: Articles', {tags: "@visual"}, () => {
            cy.visit('/articles-videos/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Articles');
        })

        it ('Unregistered: Article', {tags: "@visual"}, () => {
            cy.visit('https://ohi-s.com/articles-videos/polyetheretherketone-in-implant-prosthodontics-a-scoping-review/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Article');
        })

        it ('Unregistered: Articles by Tag', {tags: "@visual"}, () => {
            cy.visit('/articles-videos/tag/implantation/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Articles by Tag');
        })

        it ('Unregistered: Articles by Category', {tags: "@visual"}, () => {
            cy.visit('/articles-videos/category/prosthodontics/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Articles by Tag');
        })

        it ('Unregistered: Articles by Category', {tags: "@visual"}, () => {
            cy.visit('/social/people/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: /social/people/');
        })
    })
    context('Social', { tags: "@visual"}, () => {
        it ('Unregistered: People', { tags: "@visual"}, () =>{
            cy.visit('/social/people/?page=1&per_page=12&updated_at=desc')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: People');
        })

        it ('Unregistered: Lecturers profile', { tags: "@visual"}, () =>{
            cy.visit('/social/profile/10115/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Lecturers profile');
        })

        it ('Unregistered: Social profile', { tags: "@visual"}, () =>{
            cy.visit('/social/profile/13337/')
            cy.wait(5000)
            cy.percySnapshot('Unregistered: Social profile');
        })
    })
    /*
        it ('Неаторизованный: Каталог (выбран навык)+ категория + фильтры онлайн-уроками + язык', { tags: "@visual"}, () =>{
            cy.visit('/courses/orthodontics/?page=1&per_page=12&updated_at=desc&product_types=webinar&skill_slugs=braces-3&languages=en&category_slugs=orthodontics');

            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: Каталог (выбран навык)+ категория + фильтры онлайн-уроками + язык');
        })

        it ('Неаторизованный: Каталог с открытым поп-апом добавления в корзину онлайна', { tags: "@visual"}, () =>{
            cy.visit('/learning/');
            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.wait(5000);
            cy.percySnapshot('Неаторизованный: Каталог с открытым поп-апом добавления в корзину онлайна');
        })

        it ('Неаторизованный: PDP урока онлайн с поп-апом добавления в корзину', { tags: "@visual"}, () =>{
            cy.visit('/webinars/12588/');
            cy.getQa('o-content-block__wrapper').eq(0).within(() => {
                cy.get('.right-bar__button-buy').click()
            })
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP урока онлайн с поп-апом добавления в корзину');
        })

        it ('Неаторизованный: PDP урока с открытым для просмотра трейлером', { tags: "@visual"}, () =>{
            cy.visit('/webinars/12588/');
            cy.getQa('product-side-bar__preview').click()
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP урока с открытым для просмотра трейлером');
        })

        it ('Неаторизованный: PDP курса с поп-апом добавления в корзину', { tags: "@visual"}, () =>{
            cy.visit('/webinars/12588/');
            cy.getQa('right-bar_add-to-cart').click()
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP курса с поп-апом добавления в корзину');
        })

        it ('Неаторизованный: PDP курса с частично добавленными в корзину уроками', { tags: "@visual"}, () =>{
            cy.visit('/webinars-courses/157525/');
            cy.getQa('page-program').eq(1).within(() => {
                cy.getQa('add-cart-btn').eq(0).click()
            })
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP курса с частично добавленными в корзину уроками');
        })

        it ('Неаторизованный: PDP курса с поп-апом добавления в корзину', { tags: "@visual"}, () =>{
            cy.visit('/webinars/12588/');
            cy.getQa('right-bar_add-to-cart').click()
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP курса с поп-апом добавления в корзину');
        })

        it ('Неаторизованный: PDP курса с открытым поп-апом трейлеров', { tags: "@visual"}, () =>{
            cy.visit('/webinars/12588/');
            cy.getQa('product-side-bar__preview').click()
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP курса с открытым поп-апом трейлеров');
        })

        it ('Неаторизованный: PDP офлайна с поп-апом добавления в корзину', { tags: "@visual"}, () =>{
            cy.visit('/seminar/645/');
            cy.getQa('right-bar_add-to-cart').within(() => {
                cy.get('.el-button').eq(2).click()
            })
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP офлайна с поп-апом добавления в корзину');
        })

        it ('Неаторизованный: PDP офлайна с открытым поп-апом трейлера', { tags: "@visual"}, () =>{
            cy.visit('/seminar/645/');
            cy.getQa('product-side-bar__preview').click()
            cy.wait(5000)
            cy.percySnapshot('Неаторизованный: PDP офлайна');
        })
    */
})

