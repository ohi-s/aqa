describe.skip('Webinar', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    context('Действия с вебом', () => {
        it('Отображается: название веба, Краткое описание, ' +
            'Блок с ценой и трейлером, Урок входит в курс, ' +
            'Поможем с выбором, Программа, Лекторы, ' +
            'Рейтинг, Сертификат, Остались вопросы, Часто задаваемые вопросы, ' +
            'Вам может быть интересно', () => {
            cy.visit('webinars/33592/');

            cy.get('.page-header__title').should('contain', 'Для автотестов 2');//название веба
            cy.get('.page-header__content').should('be.visible');//Краткое описание
            cy.get('.o-content-block__wrapper').should('be.visible');// блок с ценой и трейлером
            cy.get('.product-page-courses__content',{timeout: 5000}).should('contain', 'Цикл для автотестов')//блок Урок входит в курс
            cy.getQa('skills-list').should('contain','Marketing');//навыки
            cy.getQa('lead-form-base').should('be.visible');//Поможем с выбором
            cy.getQa('page-program').should('be.visible');//Программа
            cy.getQa('lecturers-block').should('be.visible');//Лекторы
            //cy.get('.show-rate').should('contain', 'Reviews');//Рейтинг
            cy.get('.certificate-description').should('contain', 'вуцамкпренонгаьтртач');//описание сертификата
            cy.getQa('product-sertificate').should('be.visible');//Сертификат
            cy.getQa('lead-form-base').should('contain', 'Do you have any questions?');//Остались вопросы
            cy.getQa('FAQ-block').should('contain', 'Frequently asked questions');//Часто задаваемые вопросы
        })
    })

    context('Действия с корзиной', () => {
        beforeEach(() => {
            cy.clearCart();
        })
        it('Блок с ценой и трейлером: добавляется в корзину при нажатии на Добавить в корзину', () => {
            cy.visit('/webinars/33592/');

            cy.get('.right-bar__add-to-cart')
                .click();

            cy.get('.el-dialog__header')
                .should('contain', 'Lesson added to cart');
        })

        it('Блок с ценой и трейлером: переход на страницу payments при нажатии на Купить сейчас', () => {
            cy.visit('/webinars/33592/');

            cy.get('.right-bar__add-to-cart').click();//delete after add data-test
            cy.get('.cart__pay-button').click();

            cy.url().should('contain',`/payments/`);
        })
    })
    it('Переход на страницу оплат на странице продукта', () => {
        cy.visit('/seminar/562/');

        cy.get('.right-bar__button-buy').should('be.visible').click({force:true});

        cy.url().should('contain','/payments/');
    })
})


