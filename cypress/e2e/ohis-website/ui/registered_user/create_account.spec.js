import {randomString} from "../../../../utils";

describe('Сreate_account', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Успешная регистрация при вводе всех данных и нажатии Создать аккаунт', () => {
        cy.visit('/registration/')

        cy.wait(8000);
        cy.get('.el-form-item__content ').eq(0).type('Test');
        cy.get('.el-form-item__content ').eq(1).type('Testov');
        cy.get('.el-form-item__content ').eq(2).clear().click().type('+375333123123');
        cy.getQa('email-field').type(randomString());
        cy.get('.el-form-item__content ').eq(4).type('12345678');
        cy.getQa('signup-btn').click();

        //cy.url().should('contain', `/`);
        cy.getQa('avatar-btn').should('be.visible')
    })
})


