describe('Main page', { tags: ['@registered', 'main_page'] }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Отображается блок главный слайдер', () => {
        cy.visit('/');

        cy.get('.banner-slide').should('be.visible')
    })

    it('Отображается блок категории', () => {
        cy.visit('/');

        cy.get('.main-categories').should('be.visible')
    })

    it('Отображается блок Популярные курсы', () => {
        cy.visit('/');

        cy.get('.product-slider').eq(0).should('be.visible')
    })

    it('Отображается блок Навыки', () => {
        cy.visit('/');

        cy.get('.skills-wrapper').should('be.visible')
    })

    it('Отображается блок Скидки', () => {
        cy.visit('/');

        cy.get('.product-slider', {timeout: 5000}).eq(1).should('be.visible')
    })

    it('Отображается блок Подписка', () => {
        cy.visit('/');

        cy.getQa('subscriptions-block').should('be.visible')
    })

    it('Отображается блок Отзывы', () => {
        cy.visit('/');

        cy.get('.review-slide').should('be.visible')
    })

    it('Отображается блок Востребованные и популярные лекторы', () => {
        cy.visit('/');

        cy.get('.lecturer-card').should('be.visible')
    })

    it('Отображается блок Востребованные и популярные лекторы', () => {
        cy.visit('/');

        cy.get('.lecturer-card').eq(0).click();

        cy.url().should('contain',`/profile/`);
    })

    it('Отображается блок Остались вопросы?', () => {
        cy.visit('/');

        cy.get('.lead-form-base').should('be.visible')
    })
})


