describe.skip('Lecturers', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Categories: Orthodontics courses displayed when Orthodontics is selected', () => {
        cy.visit('/lecturers');

        cy.get('#tab-orthodontics').click();

        cy.url().should('contain',`/lecturers/orthodontics/`);
    })

    it('Categories: Remaining categories are displayed when clicking on the three dots', () => {
        cy.visit('/lecturers');

        cy.get('.expand-button').click();

        cy.get('.el-dropdown-menu__item').should('contain', 'Marketing and Management');
    })

    it('Sort by: Rating selected by default', () => {
        cy.visit('/lecturers');

        cy.getQa('sort-menu')
            .eq(0)
            .should('contain', 'Rating')
            .should('have.css', 'font-weight', '500')
    })

    it('Sort by: Lecturers are sorted by number of products when selecting by number of products', () => {
        cy.visit('/lecturers');

        cy.getQa('sort-menu').eq(4).click()

        cy.getQa('sort-menu')
            .should('contain', 'Number of products')
            .should('have.css', 'font-weight', '500')
    })

    it('Sort by: Lecturers are sorted by Name when selected by Name', () => {
        cy.visit('/lecturers');

        cy.getQa('sort-menu').eq(5).click()

        cy.getQa('sort-menu')
            .should('contain', 'Name')
            .should('have.css', 'font-weight', '500')
    })

    it('Opens the lecturer\'s profile when clicking on the lecturer', () => {
        cy.visit('/lecturers');

        cy.get('.lecturer-card__photo').eq(0).click();

        cy.url().should('contain',`/profile/`);
    })

    it('Tab information opens by default', () => {
        cy.visit('/profile/9620/');

        cy.get('#tab-general_form').should('have.attr', 'aria-selected', 'true')
    })

    it('Opens online training by clicking on online training', () => {
        cy.visit('/profile/9620/');

        cy.get('#tab-lessons').click();

        cy.get('#tab-lessons').should('have.attr', 'aria-selected', 'true')
    })

    it('Opens seminars and congresses by clicking seminars and congresses', () => {
        cy.visit('/profile/9620/');

        cy.get('#tab-offline_events').click();

        cy.get('#tab-offline_events').should('have.attr', 'aria-selected', 'true')
    })

    it('Search field: lecturer is found when entering the name of the lecturer', () => {
        cy.visit('/lecturers');


        cy.getQa('filter-content').within(() => {
            cy.getQa('filter-search-field').eq(0).type('dania');
        })

        cy.get('.lecturer-card__name').should('contain', 'Dania Tamimi');
    })
})


