describe.skip('Cart', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Переход в Payments при нажатии на кнопку "Перейти к оплате"', () => {
        cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

        cy.getQa('add-cart-btn').eq(0).click({force: true});
        cy.visit('https://release.ohi-s.com/cart/')

        cy.get('.cart-right-bar__controls').within(() => {
            cy.get('.el-button').click();
        })

        cy.url().should('contain','/payments/');
    })

    it('Применяется цена по подписке', () => {


    })

    it('Списание части баллов', () => {


    })

    it('Цена меняется при вводе промокода и нажатии на кнопку "Применить"', () => {


    })

    it('Цена меняется при вводе промокода и нажатии на кнопку "Применить"', () => {


    })


})


