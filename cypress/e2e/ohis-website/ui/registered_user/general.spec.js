describe('General', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'sergey.kovalyk@yandex.ru', password: '12345678' })
    })

    it('При нажатии на аваторку пользователя разворачивается меню пользователя', () => {
        cy.visit('/');

        cy.get('.o-user-menu-item').should('contain', 'My learning')
    })

    it('При нажатии на "Мое обучение" открывается страница доступов', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('My learning').click();

        cy.url().should('include',`/accesses/`);
        cy.getQa('paid-courses-title').should('contain', 'All my courses');
    })

    it('Открывается страница Статьи и видео при нажатии Статьи и видео', () => {
        cy.visit('/accesses/');

        cy.wait(2000);
        cy.contains('Articles & Videos').click();

        cy.url().should('include',`/articles-videos/`);
        cy.getQa('paid-courses-title').should('contain', 'Articles & videos');
    })

    it('При нажатии на "Моя подписка" открывается раздел "Моя подписка', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('My subscription').click();

        cy.url().should('include',`/my-subscription/`);
        cy.getQa('subscription-title').should('contain', 'My subscription and lessons balance');
    })

    it('При нажатии на "Мой контент" открывается раздел "Мой контент"', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('My content').click();

        cy.url().should('include',`/blog/`);
        cy.getQa('blog-create-article-btn').should('be.visible');
    })

    it('При нажатии на "Мои контакты" открывается раздел "Мои контакты"', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('My contacts').click();

        cy.url().should('include',`/social/my-contacts/`);
        cy.getQa('my-contacts__title').should('contain', 'My contacts');
    })

    it('При нажатии на "Сообщения" открывается раздел "Сообщения"', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('Messages').click();

        cy.url().should('include',`/chat/`);
        cy.get('.o-typography__heading1').should('contain', 'Messages');
    })

    it('При нажатии на "Мои финансы" открывается раздел "Мои финансы"', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('My finance').click();

        cy.url().should('include',`/finance/`);
        cy.get('.finance-banner').should('contain', 'points available');
        cy.get('.finance-table').should('contain', 'List of operations');
    })

    it('При нажатии на "Добавить компанию" открывается страница создания компании', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('Add Company').click();

        cy.url().should('include',`/create-company/`);
        cy.get('.o-typography__heading1').should('contain', 'Create Company Page');
    })

    it('При нажатии на "Настройки" открывается страница с настройками', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('Settings').click();

        cy.url().should('include',`/settings/general/`);
        cy.get('.settings-general').should('be.visible');
    })

    it('Отображается компонент переключения языка', () => {
        cy.visit('/');

        cy.get('.o-locales-select').should('be.visible');
    })

    it('Отображается компонент переключения валюты', () => {
        cy.visit('/');

        cy.get('.o-currency-select').should('be.visible');
    })

    it.skip('При нажатии на аваторку пользователя и нажатии на "Помощь?" открывается чат с Support User', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('Help').click();

        //cy.get('.o-typography__heading1').should('contain', 'Messages');
        cy.url().should('contain',`/chat/`);//?id=8557
    })

    it('При нажатии на "Выход" пользователь выходит из кабинета и отображается кнопка Войти', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.o-user-navigation').click();
        cy.contains('Log out').click();

        cy.getQa('login-btn').should('be.visible');
    })
})


