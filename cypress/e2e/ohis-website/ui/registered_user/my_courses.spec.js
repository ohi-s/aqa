describe.skip('My courses', { tags: ['@registered', 'my_courses'] }, () => {
    context('Пустая страница', () => {
        beforeEach(() => {
            cy.viewport("macbook-15")
            cy.login({ email: 'cypress.test+123123@test.com', password: 'password' })
        })
        it('Отображается пустая страница при отсутствии купленных уроков', () => {
            cy.visit('/accesses/');

            cy.get('.text__title').should('contain', "You don't have any products available yet");
        })
    })
    context('Страница с курсами', () => {
        beforeEach(() => {
            cy.viewport("macbook-15")
            cy.login({ email: 'cypress.accesses@test.com', password: 'Newpassword21!' })
        })
        it('Вебинар отображается в моих курсах после покупки Вебинар', () => {
            cy.visit('/accesses/');

            cy.getQa('sort-menu').within(() => {
                cy.contains('Lessons').click();
            })

            cy.get('.product-card__content-wrapper').should('contain', 'Для автотестов 1');
        })

        it('Курс отображается в моих курсах после покупки курса', () => {
            cy.visit('/accesses/');

            cy.get('.product-card__content-wrapper').should('contain', 'Цикл для автотесто');
        })

        it('Вебинар можно найти в поиске', () => {
            cy.visit('/accesses/');

            cy.get('.main').within(() => {
                cy.get('.search-form__input').eq(1).type('Для автотестов 1');
            });

            cy.get('.product-card__content-wrapper').should('contain', 'Для автотестов 1');
        })

        it('Курс можно найти в поиске', () => {
            cy.visit('/accesses/');

            cy.get('.main').within(() => {
                cy.get('.search-form__input').eq(1).type('Цикл для автотестов');
            });

            cy.get('.product-card__content-wrapper').should('contain', 'Цикл для автотестов');
        })

        it('Отображается кнопка скачать сертификат', () => {
            cy.visit('/accesses/');

            cy.get('.accesses-all__card-actions-certificate').should('be.visible');
        })

        it.skip('Сортировка по: покупке выбрана по умолчанию', () => {
            cy.visit('/accesses/');

            cy.get('.accesses-all__card-actions-certificate').should('be.visible');
        })

        it.skip('Сортировка по: дате', () => {
            cy.visit('/accesses/');

            cy.get('.accesses-all__card-actions-certificate').should('be.visible');
        })

        it.skip('Переход на профиль лектора при нажатии на лектора', () => {
            cy.visit('/accesses/');

            cy.get('lecture-link').click()

            cy.url().should('contain','/profile/');
        })

        it('Открываются все курсы при нажатии на Показать все уроки', () => {
            cy.visit('/accesses/');

            cy.get('.show-lessons').eq(0).click();

            cy.get('.product-card__content-wrapper').should('contain', 'Для автотестов 1');
        })

        it('Открывается урок при нажатии на урок которой в показать все', () => {
            cy.visit('/accesses/');

            cy.get('.show-lessons').eq(0).click();


            cy.get('.accesses-all__card-actions-certificate').should('be.visible');
        })

        it('открывается поп-ап Оставить отзыв при нажатии оставить отзыв', () => {
            cy.visit('/accesses/');

            cy.get('.show-lessons').eq(0).click();
            cy.get('.footer-controls-btn').eq(2).click()

            cy.get('.el-dialog__header').should('contain', 'Leave feedback');
        })

        it('Уроки сворачиваются при нажатии на Свернуть все уроки', () => {
            cy.visit('/accesses/');

            cy.get('.show-lessons').eq(0).dblclick();

            cy.contains('Для автотестов 1').should('not.be.visible');
        })

        it.skip('Открывается модальное окно при нажатии на троеточие ', () => {
            cy.visit('/accesses/');

            cy.get('.access__expand-button').eq(1).click()

            cy.get('.el-dropdown-menu').should('be.visible');
        })

        it.skip('Окно содержит 3 кнопки Перейти к просмотру, Перейти к описанию, Нужна помощь?', () => {
            cy.visit('/accesses/');

            cy.get('.accesses__dropdown-menu').click();

            cy.get('.el-dropdown-menu').should('contain', 'Go to view');
            cy.get('.el-dropdown-menu').should('contain', 'Go to description');
            cy.get('.el-dropdown-menu').should('contain', 'Need help?');
        })

        it.skip('Открывается урок при нажатии Перейти к просмотру', () => {
            cy.visit('/accesses/');

            cy.get('.accesses__dropdown-menu').click();

            cy.get('.el-dropdown-menu').should('contain', 'Go to view');
        })

        it.skip('Открывается урок при нажатии Перейти к описанию', () => {
            cy.visit('/accesses/');

            cy.get('.accesses-all__card-actions-certificate').should('be.visible');
        })

        it.skip('Открывается чат при нажатии Нужна помощь?', () => {
            cy.visit('/accesses/');

            cy.get('.accesses-all__card-actions-certificate').should('be.visible');
        })
    })
})
