describe.skip('Cart', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
        cy.clearCart()
    })

    context('Действия с корзиной в каталоге', () => {
        it('Открывается поп-ап Урок добавлен в корзину при нажатии в корзину (урок)', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});

            cy.get('[data-test="Lesson added to cart-title"]').should('contain', 'Lesson added to cart')
        })

        it('Поп-ап Урок добавлен в корзину: переход в корзину при нажатии Перейти в корзину', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.contains('Go to cart').click()

            cy.url().should('contain','/cart/');
        })

        it('Поп-ап Урок добавлен в корзину: страница оплаты при нажатии Купить сейчас', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.contains('Buy now').click();

            cy.url().should('contain','/payments/');
        })

        it('Поп-ап Урок добавлен в корзину: открывается весь курс при клике на курс', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.get('.webinar-courses__item').click();

            cy.url().should('contain','/webinars-courses/');
        })
    })

    context('Переход в оплаты', () => {
        it('Переход на страницу оплат на странице онлайн обучении', () => {
            cy.visit('/courses/?page=1&product_types=webinar_cycle');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.get('.el-dialog__body').within(() => {
                cy.contains('Buy now').click();
            })

            cy.url().should('contain','/payments/');
        })

        it('Переход на страницу оплат на странице офлайн обучения', () => {
            cy.visit('/seminars/?page=1&product_types=seminar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.get('.el-dialog__body').within(() => {
                cy.contains('Buy now').click();
            })

            cy.url().should('contain','/payments/');
        })
    })

})


