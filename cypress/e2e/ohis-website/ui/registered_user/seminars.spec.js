describe.skip('Seminars', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Категории: отображаются курсы по Ортодонтии при выборе Ортодонтия', () => {
        cy.visit('/seminars/');

        cy.get('#tab-orthodontics').click();

        cy.url().should('contain',`/learning/orthodontics/`);
    })

    it('Категории: Отображаются все категории при клике на троеточие', () => {
        cy.visit('/seminars/');

        cy.get('.expand-button').click();

        cy.get('.el-dropdown-menu__item').should('contain', 'Marketing and Management');
    })

    it('Категории: отображаются курсы из выбранной категории при клике на категорию из выпадающего списка', () => {
        cy.visit('/seminars/');

        cy.get('.expand-button').click();

        cy.contains('Marketing and Management', {timeout: 5000}).click({force: true});

        cy.url().should('contain',`/marketing-and-management/`);
    })

    it('Популярные курсы: открывается курс при клике на курс', () => {
        cy.visit('/seminars/');

        cy.get('body').then((body) => {
            if (body.find('[data-test="popular-product-image"]').length > 0) {
                cy.getQa('popular-product-image').click();

                cy.url().should('contain',`/seminars/`);
            }
            else {
                cy.log('Популярные курсы отсутсвуют');
            }
        })
    })

    it('Популярные курсы: курсы листаются при нажатии на стрелки влево и вправо', () => {
        cy.visit('/seminars/');

        cy.get('body').then((body) => {
            if (body.find('[data-test="popular-product-image"]').length > 0) {
                cy.get('.slider-button slider-button-next').click()
                cy.get('.slider-button slider-button-prev').click()
            }
            else {
                cy.log('Популярные курсы отсутсвуют');
            }
        })
    })

    it('Сортировать по: обновлению выбрано по умолчанию', () => {
        cy.visit('/seminars/');

        cy.contains('Last updated').should('have.css', 'font-weight', '500')
    })

    it('Сортировать по: курсы сортируется по дате при выборе по Дате', () => {
        cy.visit('/seminars/');

        cy.getQa('sort-menu').eq(4).click()

        cy.url().should('contain','start_at=desc');
    })

    it('Сортировать по: курсы сортируется по дате при выборе по Цене', () => {
        cy.visit('/seminars/');

        cy.getQa('sort-menu').should('be.visible');
        cy.getQa('sort-menu').eq(5).click();

        cy.url().should('contain','price=asc');
    })

    it('Открывает курс при нажатии на курс', () => {
        cy.visit('/seminars/');

        cy.getQa('product-card').within(() => {
            cy.getQa('product-image').eq(0).click({force: true});

        })

        cy.url().should('contain','/seminar/');
    })

    it('Поле поиска: курс находится при вводе названия курс', () => {
        cy.visit('/seminars/');

        cy.get('.el-input__inner').eq(0).type('Slavicek');
        cy.get('.content-item').within(() => {
            cy.get('.global-search-product-item').eq(0).click()
        })

        cy.url().should('contain', 'seminars')
    })
})
