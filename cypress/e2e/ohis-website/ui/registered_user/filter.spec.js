describe('Filter', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Courses: displayed filter Rating', () => {
        cy.visit('/courses/');

        cy.getQa('collapse-Rating-filter').should('be.visible')
    })

    it('Seminars: displayed filter Rating', () => {
        cy.visit('/seminars/');

        cy.getQa('collapse-Rating-filter').should('be.visible')
    })

    it('Lecturers: displayed filter Rating', () => {
        cy.visit('/lecturers/');

        cy.getQa('collapse-Rating-filter').should('be.visible')
    })
})


