describe.skip('Online_learning', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Категории: отображаются курсы по Ортодонтии при выборе Ортодонтия', () => {
        cy.visit('/courses/');

        cy.get('#tab-orthodontics', {timeout: 5000}).click();

        cy.url().should('contain',`/learning/orthodontics/`);
    })

    it('Категории: Отображаются все категории при клике на троеточие', () => {
        cy.visit('/courses/');

        cy.get('.expand-button').click();

        cy.get('.el-dropdown-menu__item').should('contain', 'Marketing and Management');
    })

    it('Категории: отображаются курсы из выбранной категории при клике на категорию из выпадающего списка', () => {
        cy.visit('/courses/');

        cy.get('.expand-button').click();

        cy.contains('Marketing and Management', {timeout: 5000}).click({force: true});

        cy.url().should('contain',`/marketing-and-management/`);
    })

    it('Популярные курсы: открывается курс при клике на курс', () => {
        cy.visit('/courses/');

        cy.get('body').then((body) => {
            if (body.find('.product-slider').length > 0) {
                cy.get('.product-slider').within(() => {
                    cy.get('.product-slide__image').eq(0).click();
                })

                cy.url().should('contain',`/webinars-courses/`);
            }
            else {
                cy.log('Популярные курсы отсутсвуют');
            }
        })
    })

    it('Популярные курсы: курсы листаются при нажатии на стрелки влево и вправо', () => {
        cy.visit('/courses/');

        cy.get('body').then((body) => {
            if (body.find('.product-slider').length > 0) {
                cy.get('.product-slider__actions').within(() => {
                    cy.get('.actions__button--next').click()
                    cy.get('.actions__button--prev').click()
                })
            }
            else {
                cy.log('Популярные курсы отсутсвуют');
            }
        })

    })

    it('Сортировать по: обновлению выбрано по умолчанию', () => {
        cy.visit('/courses/');

        cy.contains('Last updated').should('have.css', 'font-weight', '500')
    })

    it('Сортировать по: курсы сортируется по дате при выборе по Дате', () => {
        cy.visit('/courses/');

        cy.getQa('sort-menu').eq(4).click()

        cy.url().should('contain','start_at=desc');
        })

    it('Сортировать по: курсы сортируется по дате при выборе по Цене', () => {
        cy.visit('/courses/');

        cy.getQa('sort-menu').should('be.visible')
        cy.getQa('sort-menu').eq(5).click()

        cy.url().should('contain','price=asc');
    })

    it('Отображать: Курсами выбрано по умолчанию', () => {
        cy.visit('/courses/');

        cy.contains('Courses').should('have.css', 'font-weight', '700')
        cy.url().should('contain','/courses/');

    })

    it('Отображать: отображается уроками при нажатии на уроками', () => {
        cy.visit('/courses/');

        cy.get('.webinars-courses-toggle').within(() => {
            cy.contains('Webinars', {timeout: 5000}).click()
        })

        cy.url().should('contain','product_types=webinar');
    })

    it('Открывает урок при нажатии на урок', () => {
        cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

        cy.get('.product-card__right-block').eq(0).click();

        cy.url().should('contain','/webinars/');
    })

    it('Открывает курс при нажатии на курс', () => {
        cy.visit('/courses/');

        cy.get('.product-card__right-block').eq(0).click();

        cy.url().should('contain','/webinars-courses/');
    })

    // it('Поле поиска: вебинар находится при вводе названия курс', () => {
    //     cy.visit('/courses/');
    //
    //     cy.get('.el-input__inner').eq(0).type('для автотестов');
    //     cy.get('.content-item').within(() => {
    //         cy.get('.global-search-product-item').eq(0).click()
    //     })
    //
    //     cy.url().should('contain', '/webinars/')
    // })

})


