describe('Footer', () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('При нажатии на ссылку "Мое обучение" переход на страницу моего обучения', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('My learning').click()
        })

        cy.url().should('include',`/accesses/`);
    })

    it('При нажатии на ссылку "Мой контент" переход на  страницу моего контента', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('My content').click()
        })

        cy.url().should('include',`/blog/`);
    })

    it('При нажатии на ссылку "Мои контакты" переход на  страницу моих контактактов', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('My contacts').click()
        })

        cy.url().should('include',`/my-contacts/`);
    })
})
