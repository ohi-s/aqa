describe.skip('Seminar', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Отображается: название семинара, Краткое описание, ' +
        'Блок с ценой и трейлером, Навыки, ' +
        'Поможем с выбором, Программа, Лекторы, ' +
        'Рейтинг ожидания, Сертификат, Остались вопросы, Часто задаваемые вопросы, ' +
        'Вам может быть интересно', () => {
        cy.visit('/seminar/625/');

        cy.get('.page-header__title').should('contain', 'Семинар для автотестов');//название семинара
        cy.get('.page-header__content').should('be.visible');//Краткое описание
        cy.get('.o-content-block__wrapper').should('be.visible');// блок с ценой и трейлером
        cy.getQa('skills-list').should('contain','Marketing');//навыки
        //cy.getQa('lead-form-base').should('be.visible');//Поможем с выбором
        cy.getQa('page-program').should('contain', 'автотест');//Программа
        cy.getQa('lecturers-block').should('contain', ' Alessandro Devigus');//Лекторы
        //cy.get('.expectations-rate').should('contain', 'Expectations rating');//Рейтинг ожидания
        cy.get('.certificate-description').should('contain', 'акуфпероенвочлнпглнплон');//описание Сертификат
        cy.getQa('product-sertificate').should('be.visible');//Сертификат
        cy.getQa('lead-form-base').should('contain', 'Do you have any questions?');//Остались вопросы
        cy.getQa('FAQ-block').should('contain', 'Frequently asked questions');//Часто задаваемые вопросы
    })

    it('Блок с ценой и трейлером: добавляется в корзину при нажатии на Добавить в корзину', () => {
        cy.visit('/seminar/625/');

        cy.get('.right-bar__add-to-cart')
            .click();

        cy.get('.el-dialog__header')
            .should('contain', 'Seminar added to cart');
    })

    it('Блок с ценой и трейлером: переход на страницу payments при нажатии на Купить сейчас', () => {
        cy.visit('/seminar/625/');

        cy.get('.right-bar__add-to-cart').click();//delete after add data-test
        cy.get('.cart__pay-button').click();

        cy.url().should('contain',`/payments/`);
    })
})


