describe('Webinar-courses', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15");
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    context('Действия с курсом', () => {
        it('Отображается: название курса', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('.page-header__title').should('contain', 'Цикл для автотестов');
        })

        it('Отображается: Краткое описание', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('.page-header__content').should('be.visible');
        })

        it('Отображается: Блок с ценой и трейлером', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('.o-content-block__wrapper').should('be.visible');
        })

        it('Отображается: Навыки', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('skills-list').should('contain','Marketing');
        })

        it('Отображается: трейлеры', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('.trailer-carousel__title', {timeout: 7000}).should('be.visible');
        })

        it('Отображается: Описание', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('page-program').should('contain', 'Online library');
        })

        it('Отображается: Поможем с выбором', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('lead-form-base').should('be.visible');
        })

        it('Отображается: Программа уроков', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('page-program').should('be.visible');
        })

        it('Отображается: Лекторы', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('lecturers-block').should('be.visible');
        })

        it('Отображается: Описание сертификата', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('.certificate-description').should('contain', 'Recommended for');
        })

        it('Отображается: Сертификат', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('product-sertificate').should('be.visible');
        })

        it('Отображается: Остались вопросы', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('lead-form-base').should('contain', 'Do you have any questions?');
        })

        it('Отображается: Часто задаваемые вопросы', () => {
            cy.visit('webinars-courses/157653/');

            cy.getQa('FAQ-block').should('contain', 'Frequently asked questions');
        })

        it.skip('Отображается: Вам может быть интересно', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('.product-page__interesting').should('be.visible')
        })

        it.skip('Отображается: Рейтинг', () => {
            cy.visit('webinars-courses/157653/');

            //cy.get('.show-rate').should('contain', 'Reviews');//Рейтинг
        })
    })

    context.skip('Действия с корзиной', () => {
        beforeEach(() => {
            cy.clearCart();
        })
        it('Блок с ценой и трейлером: открывается модальное окно с выбором уроков', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('[data-test="right-bar_actions"]').within(() => {
                cy.contains('Select lessons').click();
            })

            cy.get('.el-drawer__header')
                .should('contain', 'Adding to cart');
        })

        it('Блок с ценой и трейлером: переход на страницу payments при нажатии на Купить сейчас', () => {
            cy.visit('webinars-courses/157653/');

            cy.get('[data-test="right-bar_actions"]').within(() => {
                cy.contains('Buy a course').click();
            })

            cy.url().should('contain',`/payments/`);
        })
    })
})


