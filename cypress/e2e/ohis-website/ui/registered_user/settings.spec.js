describe.skip('Organization', { tags: '@registered' }, () => {
    beforeEach(() => {
        //cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Organizations page displays organizations', () => {
        cy.visit('/companies');

        cy.getQa('company-logo-img').should('be.visible')
    })

    it('Transition to the organization by clicking on the logo of the organization', () => {
        cy.visit('/companies');

        cy.get('.company-logo').eq(0).click()
        cy.get('.organization-header').should('be.visible')
    })


})


