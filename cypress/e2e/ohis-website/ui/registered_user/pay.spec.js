import {
    paymentStripe,
} from "../../../../utils";

describe('Pay', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

    it('Подписка успешно оплачивается по прямой ссылке', () => {
        cy.visit('payments/?plan_id=1');

        paymentStripe();
        cy.get('.el-checkbox__inner').click();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','/cart/success/');
        cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
        cy.get('.cart-success__order-status').should('contain', 'Payment received');
        cy.get('.cart-success__order-item').should('contain', 'Tariff plan “2 lessons“');
    })

    it('Курс успешно оплачивается по прямой ссылке', () => {
        cy.visit('payments/?route=%2Fwebinars-courses%2F168038%2F&id=168038&quantity=1&fastBuy=true');

        paymentStripe();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','/cart/success/');
        cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
        cy.get('.cart-success__order-status').should('contain', 'Payment received');
        //в курсе не выводится несколько раз покупака тех же вебов
        //cy.get('.cart-success__order-item').should('contain', 'Webinar “Introduction to dental marketing and its impact on the dental industry“');
        //cy.get('.cart-success__order-item').should('contain', 'Webinar “Internal marketing for your dental business “');
    })

    it('Веб успешно оплачивается по прямой ссылке', () => {
        cy.visit('payments/?route=%2Fwebinars%2F167901%2F&id=167901&quantity=1&fastBuy=true');

        paymentStripe();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','/cart/success/');
        cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
        cy.get('.cart-success__order-status').should('contain', 'Payment received');
        cy.get('.cart-success__order-item').should('contain', 'Webinar “What every dentist needs to know about the management of temporomandibular disorders. The facts and the fantasies. Part 1“');
    })

    it('Семинар успешно оплачивается по прямой ссылке', () => {
        cy.visit('payments/?route=%2Fseminar%2F168112%2F&id=168112&quantity=1&fastBuy=true');

        paymentStripe();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','/cart/success/');
        cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
        cy.get('.cart-success__order-status').should('contain', 'Payment received');
        cy.get('.cart-success__order-item').should('contain', 'Seminar “Orthodontics in pediatric dentistry: from myth to practice“');
    })

    it('Поля для ввода данных карты отсутсвуют при выборе страны "Россия"', () => {
        cy.visit('payments/?plan_id=1');

        cy.wait(5000)
        cy.get('.el-select__caret').click();
        cy.get('.el-scrollbar__view').within(() => {
            cy.contains('Russia').click();
        })

        cy.get('.card-block').should('not.exist');
    })

    it('Переход на Тиньков при выборе страна Россия', () => {
        cy.visit('payments/?plan_id=1');

        cy.wait(5000);
        cy.get('.el-checkbox').click();
        cy.get('.el-select__caret').click();
        cy.get('.el-scrollbar__view').within(() => {
            cy.contains('Russia').click();
        })
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','securepayments.tinkoff.ru/');
    })

    it('Поля для ввода данных карты присутсвуют при выборе страны "Беларусь"', () => {
        cy.visit('payments/?plan_id=1');

        cy.wait(5000);
        cy.get('.el-select__caret').click();
        cy.get('.el-scrollbar__view').within(() => {
            cy.contains('Belarus').click();
        })

        cy.get('.card-block').should('be.visible');
    })

    it('Отображается страница успешной оплаты при вводе корректных данных карте и попытке оплаты (Stripe)', () => {
        cy.visit('payments/?plan_id=1');

        paymentStripe();
        cy.get('.el-checkbox__inner').click();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','/cart/success/');
        cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
        cy.get('.cart-success__order-status').should('contain', 'Payment received');
        cy.get('.cart-success__order-item').should('contain', 'Tariff plan “2 lessons“');
    })

    it.skip('Отображается страница успешной оплаты при вводе корректных данных карте и попытке оплаты (Tinkoff)', () => {
        cy.visit('payments/?plan_id=1');

        paymentStripe();
        cy.get('.el-checkbox__inner').click();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','/cart/success/');
        cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
        cy.get('.cart-success__order-status').should('contain', 'Payment received');
        cy.get('.cart-success__order-item').should('contain', 'Tariff plan “2 lessons“');
    })

    it.skip('Проверить сообщения об ошибке при неудачной оплате', () => {})

    it.skip('Карта отображается в кабинете после оплаты тарифного плана подписки', () => {
        cy.visit('payments/?plan_id=1');

        paymentStripe();
        cy.get('.el-checkbox__inner').click();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.url().should('contain','/cart/success/');
        cy.visit('https://release.ohi-s.com/my-subscription/');
    })

    it('Ошибка при вводе карты с -1 цифрой', () => {
        cy.visit('payments/?plan_id=1');

        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardNumber"]',
            '424242424242424'
        )
        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardExpiry"]',
            '1026'
        )
        cy.getStripeElement('input[data-elements-stable-field-name="cardCvc"]', '123')
        cy.get('.el-checkbox__inner').click();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.get('.el-form-item__error').should('contain','Your card number is incomplete.')
    })

    it('Ошибка при вводе карты с невалидной датой', () => {
        cy.visit('payments/?plan_id=1');

        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardNumber"]',
            '4242424242424242'
        )
        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardExpiry"]',
            '1021'
        )
        cy.getStripeElement('input[data-elements-stable-field-name="cardCvc"]', '123')
        cy.get('.el-checkbox__inner').click();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.get('.el-form-item__error').should.should('contain',"Your card's expiration year is in the past.")
    })

    it('Ошибка при вводе карты с невалидным CVV', () => {
        cy.visit('payments/?plan_id=1');

        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardNumber"]',
            '4242424242424242'
        )
        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardExpiry"]',
            '1026'
        )
        cy.getStripeElement('input[data-elements-stable-field-name="cardCvc"]', '12')
        cy.get('.el-checkbox__inner').click();
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.get('.el-form-item__error').should('contain', "Your card's security code is incomplete.");
    })

    it('Ошибка при не отмеченным чек боксом', () => {
        cy.visit('payments/?plan_id=1');

        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardNumber"]',
            '4242424242424242'
        )
        cy.getStripeElement(
            'input[data-elements-stable-field-name="cardExpiry"]',
            '1026'
        )
        cy.getStripeElement('input[data-elements-stable-field-name="cardCvc"]', '123')
        cy.get('.el-button.education-payments__order-button.el-button--danger').click();

        cy.get('.el-form-item__error').should('contain', "Field is required");
    })

    //открывается в другой вкладке
    it.skip('Переход на страницу Соглашения при нажатии на пользовательское соглашение', () => {
        cy.visit('payments/?plan_id=1');

        cy.contains('offer').click();

        cy.url().should('contain','/offer/');
    })
})
