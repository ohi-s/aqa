describe.skip('Profile', { tags: '@registered' }, () => {
    beforeEach(() => {
        //cy.viewport("macbook-15")
        cy.login({ email: 'cypress.test@test.com', password: 'password' })
    })

        it('Профиль: проверка добавления изображения', () => {
        cy.visit('/profile/1034894/');

        cy.get('.profile-banner').within(() => {
            cy.get('.el-button').click();
        })

        cy.get('input[type=file]').attachFile('profileback.jpg');

        //cy.get('input[value='Open']').click();

        cy.get('.profile-banner__img').should('contain', 'profileback.jpg');
    })

        it('Профиль: проверка замены изображения', () => {
        cy.visit('/profile/1034894/');

        cy.get('.profile-banner').within(() => {
            cy.get('.el-button').click();
        })

        cy.get('input[type=file]').attachFile('profileback2.jpg');

        //cy.get('input[value='Open']').click();

        cy.get('.profile-banner__img').should('contain', 'profileback2.jpg');
    })

        it('Профиль: проверка добавления портрета', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-tooltip add-photo gray el-button--empty').click();

        cy.get('input[type=file]').attachFile('profileportrait.jpg');

        //cy.get('input[value='Open']').click();

        cy.get('.profile-info__avatar flex-shrink-0').should('contain', 'profileportrait.jpg');
    })

        it('Профиль: проверка замены портрета', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-tooltip add-photo gray el-button--empty').click();

        cy.get('input[type=file]').attachFile('profileportrait2.jpg');

        //cy.get('input[value='Open']').click();

        cy.get('.profile-info__avatar flex-shrink-0').should('contain', 'profileportrait2.jpg');
    })

        it('Профиль: проверка добавления краткого описания профиля', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button profile-info__description-button el-button--text el-button--mini').click();

        cy.getQa('el-input__inner').type('Something about myself');

        cy.get('.el-button profile-info__control-button el-button--primary el-button--small').click();

        cy.get('.profile-info__description-wrapper').should('contain', 'Something about myself');
    })

        it('Профиль: проверка изменения краткого описания профиля', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button profile-info__description-button el-button--text el-button--mini').click();

        cy.getQa('el-input__inner').type('New things about myself');

        cy.get('.el-button profile-info__control-button el-button--primary el-button--small').click();

        cy.get('.profile-info__description-wrapper').should('contain', 'New things about myself');
    })

        it.skip('Профиль: проверка изменения процента заполнения профиля', () => {
        cy.visit('/profile/1034894/');

    })

        it('Профиль: проверка добавления информации о языках пользователя', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-button--empty el-button--small').click();

        cy.get('.el-input__suffix-inner').click();

        cy.contains('Russian').click({force: true});

        cy.get('.el-button save-button el-button--primary el-button--small').click();

        cy.get('.info-text').should('contain', 'Russian');
    })

        it('Профиль: проверка изменения информации о языках пользователя', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-button--empty el-button--small').click();

        cy.get('.el-input__suffix-inner').click();

        cy.contains('English').click({force: true});

        cy.get('.el-button save-button el-button--primary el-button--small').click();

        cy.get('.info-text').should('contain', 'Russian, English');
    })

        it('Профиль: проверка добавления информации о стране пользователя', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-button--empty el-button--small').click();

        cy.get('.el-input__suffix').click();

        cy.contains('Belarus').click({force: true});

        cy.get('.el-button save-button el-button--primary el-button--small').click();

        cy.get('.info-text').should('contain', 'Belarus');
    })

        it('Профиль: проверка изменения информации о стране пользователя', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-button--empty el-button--small').click();

        cy.get('.el-input__suffix').click();

        cy.contains('Germany').click({force: true});

        cy.get('.el-button save-button el-button--primary el-button--small').click();

        cy.get('.info-text').should('contain', 'Germany');
    })

        it('Профиль: проверка добавления информации в поле "Обо мне"', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-button--empty el-button--small').click();

        cy.get('.o-textarea primary').type('Ordinary information about me');

        cy.get('.el-button save-button el-button--primary el-button--small').click();

        cy.get('.item__text custom-style-description').should('contain', 'Ordinary information about me');
    })

        it('Профиль: проверка изменения информации в поле "Обо мне"', () => {
        cy.visit('/profile/1034894/');

        cy.get('.el-button el-button--empty el-button--small').click();

        cy.get('.o-textarea primary').type('Something new about me: add info');

        cy.get('.el-button save-button el-button--primary el-button--small').click();

        cy.get('.item__text custom-style-description').should('contain', 'Something new about me: add info');
    })

        it('Профиль: проверка добавления информации в поле "Опыт работы"', () => {
        cy.visit('/profile/1034894/');

        cy.get('.work-experience__header').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.job-name-wrapper').within(() => {
            cy.getQa('o-input primary large').type('New company');
        });

        cy.get('.position-name-wrapper').within(() => {
            cy.getQa('o-input primary large').type('New position');
        });

       //как выбрать дату начала и завершения?/

        cy.get('.work-experience__header').within(() => {
            cy.get('.buttons-wrapper').within(() => {
                cy.get('.el-button').click()
            })
        });

        cy.get('.experience-text').should('contain', 'New company');
        cy.get('.experience-text').should('contain', 'New position');
    })

        it('Профиль: проверка добавления информации в поле "Образование"', () => {
        cy.visit('/profile/1034894/');

        cy.get('.profile-education__header').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.institution-name-wrapper').within(() => {
            cy.getQa('o-input primary large').type('New academy');
        });

        cy.get('.institution-certificate-wrapper').within(() => {
            cy.getQa('o-input primary large').type('New certificate');
        });

        //как выбрать дату начала и завершения?/

        cy.get('.profile-education__header').within(() => {
            cy.get('.el-button').eq(1).click()
            })
        });

        cy.get('.education-text').should('contain', 'New company');
        cy.get('.education-text').should('contain', 'New position');
    })

        it('Профиль: проверка добавления информации в поле "Я умею"', () => {
        cy.visit('/profile/1034894/');

        cy.get('.skills__empty-block').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.el-dialog').within(() => {
            cy.get('.el-checkbox').eq(0).click();
            cy.get('.el-checkbox').eq(1).click();
        });

        cy.get('.title-block').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.o-collapse-preview__content').should('contain', 'Самолигирующие брекет-системы');
    })

        it('Профиль: проверка добавления информации в поле "Хочу научиться"', () => {
        cy.visit('/profile/1034894/');

        cy.get('.skills__empty-block').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.el-dialog').within(() => {
            cy.get('.el-checkbox').eq(0).click();
            cy.get('.el-checkbox').eq(1).click();
        });

        cy.get('.title-block').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.o-collapse-preview__content').should('contain', 'Самолигирующие брекет-системы');
    })

        it('Профиль: проверка добавления информации в поле "Интересы"', () => {
        cy.visit('/profile/1034894/');

        cy.get('.item__title-wrapper').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.item__tags tags-edit').within(() => {
            cy.get('.item__tag item__tag_editable').eq(0).click();
            cy.get('.item__tag item__tag_editable').eq(1).click();
        });

        cy.get('.item__title-wrapper').within(() => {
            cy.get('.el-button').click();
        });

        cy.get('.o-collapse-preview__content').should('contain', 'Ортодонтия');
    })