describe.skip('Subscription', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    context('Без подписки', () => {
        beforeEach(() => {
            cy.login({ email: 'cypress.test@test.com', password: 'password' })
        })

        it('Открывается поп-ап при нажатии Выбран план Start', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(0).click()
            })

            cy.get('.el-dialog__header').within(() => {
                cy.get('.o-title').should('contain', 'Start')
            })
        })

        it('Открывается поп-ап при нажатии Выбран план на Basic', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(1).click()
            })

            cy.get('.el-dialog__header').within(() => {
                cy.get('.o-title').should('contain', 'Basic')
            })
        })

        it('Открывается поп-ап при нажатии Выбран план Professional', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(2).click()
            })

            cy.get('.el-dialog__header').within(() => {
                cy.get('.o-title').should('contain', 'Professional')
            })
        })

        it('Количество уроков изменяется при выборе годовой подписки 2 -> 24', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-switch__core').click()
            })

            cy.get('.subscription-card').eq(0).contains( '24 lessons')
        })

        it('Количество уроков изменяется при выборе годовой подписки 4 -> 48', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-switch__core').click()
            })

            cy.get('.subscription-card').eq(1).contains( '48 lessons')
        })

        it('Количество уроков изменяется при выборе годовой подписки 6 -> 72', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-switch__core').click()
            })

            cy.get('.subscription-card').eq(2).contains( '72 lessons')
        })

        it('Открывается поп-ап при нажатии Выбран план Start (24 LESSONS / YEAR)', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-switch__core').click()
            })
            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(0).click()
            })

            cy.get('.el-dialog__header').within(() => {
                cy.get('.o-title').should('contain', 'Start')
            })
            cy.get('.el-dialog__body').should('be.visible');
        })

        it('Открывается поп-ап при нажатии Выбран план Basic (48 LESSONS / YEAR)', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-switch__core').click()
            })
            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(1).click()
            })

            cy.get('.el-dialog__header').within(() => {
                cy.get('.o-title').should('contain', 'Basic')
            })
            cy.get('.el-dialog__body').should('be.visible');
        })


        it('Открывается поп-ап при нажатии Выбран план Professional (72 LESSONS / YEAR)', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-switch__core').click()
            })
            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(2).click()
            })

            cy.get('.el-dialog__header').within(() => {
                cy.get('.o-title').should('contain', 'Professional')
            })
            cy.get('.el-dialog__body').should('be.visible');
        })

        it.skip('Поп-ап подписка: открывается stripe при выборе Беларусь', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(0).click()
            })
            cy.get('.el-select__caret').click()
            cy.get('.el-scrollbar__view').within(() => {
                cy.contains('Belarus').click()
            })
            cy.get('.el-dialog__body').within(() => {
                cy.get('.box').click()
            })
            cy.get('.country-selection__controls').click();

            cy.url().should('include', 'checkout.stripe.com/');
        })

        it('Поп-ап подписка: открывается tinkoff при выборе Россия', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(2).click()
            })
            cy.get('.el-select__caret').click();
            cy.get('.el-scrollbar__view').within(() => {
                cy.contains('Russia').click();
            })
            cy.get('.el-dialog__body').within(() => {
                cy.get('.box').click();
            })
            cy.get('.country-selection__controls').click();

            cy.url().should('contain','securepayments.tinkoff.ru/');
        })

        it('Цена в евро по умолчанию', () => {
            cy.visit('/subscription');

            cy.getQa('EUR-menu').click({force: true});

            cy.getQa('price-view').should('contain', '€')
            cy.get('.subscription-card__price-by-period').should('contain', '€')

        })

        it('Цена в долларах при выборе USD', () => {
            cy.visit('/subscription');

            cy.getQa('USD-menu').click({force: true});

            cy.getQa('price-view').should('contain', '$')
            cy.get('.subscription-card__price-by-period').should('contain', '$')
        })

        it('Цена в рублях при выборе RUB', () => {
            cy.visit('/subscription');

            cy.getQa('RUB-menu').click({force: true});

            cy.getQa('price-view').should('contain', '₽')
            cy.get('.subscription-card__price-by-period').should('contain', '₽')
        })

        it('Поп-ап подписка: ошибка при не активном чек боксе об оферте', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(2).click()
            })
            cy.get('.el-select__caret').click();
            cy.get('.el-scrollbar__view').within(() => {
                cy.contains('Russia').click();
            })
            cy.get('.country-selection__controls').click();

            cy.get('.help').should('contain', 'The Agree to the terms is required')
        })

        it('Открывается выпадающий список стран при нажатии на стрелку', () => {
            cy.visit('/subscription');

            cy.get('.subscriptions').within(() => {
                cy.get('.el-button').eq(2).click()
            })
            cy.get('.el-select__caret').click();

            cy.contains('Afghanistan').should('be.visible');
        })
    })

    context('С подпиской', () => {
        beforeEach(() => {
            cy.login({ email: 'test.subscription@test.ru', password: 'password' })
        })

        it('Отображается страница с информацией о подписке', () => {
            cy.visit('/subscription');

            cy.getQa('main-card-info').should('be.visible');

        })

        it('Отображается кнопка подписки после покупки подпики', () => {
            cy.visit('/subscription');

            cy.getQa('subscription-btn').should('be.visible');
        })

        it('Переход на страницу оплаты при изменении карты', () => {
            cy.visit('/subscription');

            cy.getQa('edit-btn').click();
            cy.getQa('add-card-btn').click();

            cy.url().should('contain','securepayments.tinkoff.ru/');
        })

        it('Переход в каталог при нажатии онлайн обучение', () => {//fix OCV-3698
            cy.visit('/subscription');

            cy.get('.info-card__paragraph').within(() => {
                cy.contains('catalog').click();
            })

            cy.url().should('contain','learning');
        })

        it('Открывается поп-ап с выбором тарифа при нажатии Choose another tariff', () => {//fix OCV-3698
            cy.visit('/subscription');

            cy.contains('Choose another tariff').click();

            cy.getQa('subscriptions-block').should('be.visible');
        })
    })
    context('Оплаты ', () => {
        it('Подписка: Переход на страницу страйп при выборе Россия', () => {
            cy.visit('/subscription/');

            cy.get('[data-test="subscription-card-btn"]').eq(0).click();
            cy.get('.el-select__caret').click();
            cy.get('.el-scrollbar__view').within(() => {
                cy.contains('Russia').click();
            })
            cy.get('[data-test="box-btn"]').click();
            cy.get('[data-test="proceed-to-checkout-btn"]').click();

            cy.url().should('contain','securepayments.tinkoff.ru/');
        })

        it.skip('Подписка: Переход на страницу страйп при выборе Беларусь', () => {
            cy.visit('/subscription/');

            cy.get('[data-test="subscription-card-btn"]').eq(0).click();
            cy.get('.el-select__caret').click();
            cy.get('.el-scrollbar__view').within(() => {
                cy.contains('Belarus').click();
            })
            cy.get('[data-test="box-btn"]').click();
            cy.get('[data-test="proceed-to-checkout-btn"]').click();

            cy.url().should("contains", "https://checkout.stripe.com/pay/")
        })
    })
})




