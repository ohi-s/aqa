describe('Organization', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Отображается список компаний', () => {
        cy.visit('/companies');

        cy.getQa('company-logo-img').should('be.visible')
    })

    it('Отображается поле для поиска', () => {
        cy.visit('/companies');

        cy.getQa('filter-search-field').should('be.visible')
    })

    it('Отображается кнопка Create company', () => {
        cy.visit('/companies');

        cy.get('.company-catalog').within(() => {
            cy.contains('Create company').should('be.visible')
        })
    })

    it('Переход на страницу авторизации при нажатии на кнопку Create company', () => {
        cy.visit('/companies');

        cy.wait(1000);
        cy.get('.company-catalog').within(() => {
            cy.get('.el-button').eq(0).click();
        })

        cy.url().should('include',`/login/`);
    })

    it('Компания находится при частичном вводе названия компании', () => {
        cy.visit('/companies');

        cy.wait(1000);
        cy.getQa('filter-search-field').type('ooo');
        cy.get('.company-card__info').should('contain', 'OOO');
    })

    it('Переход на страницу компании при нажатии на лого компании', () => {
        cy.visit('/companies');

        cy.getQa('company-logo-img').eq(0).click()
        cy.get('.organization-header').should('be.visible')
        cy.url().should('contain',`/company/`);

    })

    it('Отображается кнопка Написать сообщение', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('write-message-btn').should('be.visible')
    })

    it('Открывается таб "Информация" по умолчанию ', () => {
        cy.visit('company/OHI-S/');

        cy.get('#tab-0').should('have.attr', 'aria-selected', 'true')
    })

    it('Отображается название компании', () => {
        cy.visit('company/OHI-S/');

        cy.get('.o-typography__heading1').should('contain', 'OHI-S');
    })

    it('Отображается количество Актуальных событий', () => {
        cy.visit('company/OHI-S/');

        cy.get('.company-events').should('contain', 'Actual events');
    })

    it('В блоке "Лекторы" отображается Общий рейтинг', () => {
        cy.visit('company/OHI-S/');

        cy.get('.company-rating__overall').should('be.visible')
    })

    it('Отображается блок Лекторы', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('lecturers-holder').should('be.visible')
    })

    it('Отображается блок Лекторы', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('lecturers-holder').should('be.visible')
    })

    it('Отображается список лекторов', () => {
        cy.visit('company/OHI-S/');

        cy.get('.lecturers-list').should('be.visible')
    })

    it('Отображается кнопка Watch All', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('watch-all-btn').should('be.visible')
    })

    it('Открывается страница лекторов компании при нажатии Watch all', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('lecturers-holder').within(() => {
            cy.getQa('watch-all-btn', {timeout: 5000})
                .should('be.visible')
                .eq(0)
                .click();
        });

        cy.url().should('contain',`lecturers/`);
    })

    it('Переход на профиль пользователя лектора при нажатии на лектора', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('lector-holder').eq(0).click();

        cy.url().should('contain',`/social/profile/`);
    })

    it('В блоке "Онлайн-обучение" отображается количество онлайн-мероприятий от организации', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('online-learning-holder').within(() => {
            cy.get('.o-typography__span').should('be.visible')
        })
    })

    it('В блоке "Онлайн-обучение" отображается список онлайн-мероприятий', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('online-learning-holder').within(() => {
            cy.get('.cards-holder').should('be.visible')
        })
    })

    it('В блоке "Онлайн-обучение" отображается кнопка "View all"', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('online-learning-holder').within(() => {
            cy.getQa('watch-all-btn').should('be.visible')
        })
    })

    it('Переход на PDP мероприятия при нажатии на него в блоке "Онлайн-обучение"', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('online-learning-holder').within(() => {
            cy.get('.product-image').eq(0).click()
        })

        cy.url().should('contain',`/webinars-courses/`);
    })

    it('Переход на PLP при нажатии на "Смотреть все"', () => {
        cy.visit('company/OHI-S/');

        cy.wait(2000);
        cy.getQa('online-learning-holder').within(() => {
            cy.getQa('watch-all-btn').eq(0).click()
        })

        cy.url().should('contain',`/company/OHI-S/courses/`);
    })

    it('В блоке "Офлайн-обучение" отображается список офлайн-мероприятий', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('offline-holder').within(() => {
            cy.get('.cards-holder').should('be.visible')
        })
    })

    it('В блоке "Офлайн-обучение" отображается список офлайн-мероприятий', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('offline-holder').within(() => {
            cy.get('.cards-holder').should('be.visible')
        })
    })

    it('В блоке "Офлайн-обучение" отображается кнопка "View all"', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('offline-holder').within(() => {
            cy.getQa('watch-all-btn').should('be.visible')
        })
    })

    it('Переход на PDP мероприятия при нажатии на него в блоке "Офлайн-обучение"', () => {
        cy.visit('company/OHI-S/');

        cy.getQa('offline-holder').within(() => {
            cy.get('.product-image').eq(0).click()
        })

        cy.url().should('contain',`/seminar/`);
    })

    it('Переход на PLP при нажатии на "Смотреть все"', () => {
        cy.visit('company/OHI-S/');

        cy.wait(2000);
        cy.getQa('offline-holder').within(() => {
            cy.getQa('watch-all-btn').eq(0).click()
        })

        cy.url().should('contain',`/seminars/`);
    })

    it('Активный таб Онлайн обучение при переходе в Онлайн обучение', () => {
        cy.visit('company/OHI-S/');

        cy.get('#tab-1').click().should('have.attr', 'aria-selected', 'true')
    })

    it('В табе "Онлайн-обучение" есть блок фильтров', () => {
        cy.visit('company/OHI-S/courses/');

        cy.getQa('filter-content').should('be.visible');
    })

    it('В табе "Онлайн-обучение" есть блок сортировок', () => {
        cy.visit('company/OHI-S/courses/');

        cy.get('.product-catalog__filter-wrapper').should('be.visible');
    })

    it('В табе "Онлайн-обучение" есть блок продуктов', () => {
        cy.visit('company/OHI-S/courses/');

        cy.get('.products').should('be.visible');
    })

    it('В табе "Онлайн-обучение" есть блок пагинации', () => {
        cy.visit('company/OHI-S/courses/');

        cy.get('.o-pagination__desktop').should('be.visible');
    })

    it('В табе "Онлайн-обучение" есть блок скиллов', () => {
        cy.visit('company/OHI-S/courses/');

        cy.get('.skills-catalog').should('be.visible');
    })

    it('Онлайн-обучение: Активный таб Семинары и конгрессы при переходе в Семинары и конгрессы', () => {
        cy.visit('company/OHI-S/courses/');

        cy.get('#tab-2').click().should('have.attr', 'aria-selected', 'true')
    })

    it('Активный таб Семинары и конгрессы при переходе в Онлайн обучение', () => {
        cy.visit('company/OHI-S/');

        cy.get('#tab-3').click().should('have.attr', 'aria-selected', 'true')
    })

    it('В табе "Семинары и конгрессы" есть блок фильтров', () => {
        cy.visit('company/OHI-S/seminars/');

        cy.getQa('filter-content').should('be.visible');
    })

    it('В табе "Семинары и конгрессы" есть блок сортировок', () => {
        cy.visit('company/OHI-S/seminars/');

        cy.get('.product-catalog__filter-wrapper').should('be.visible');
    })

    it('В табе "Семинары и конгрессы" есть блок продуктов', () => {
        cy.visit('company/OHI-S/seminars/');

        cy.get('.products').should('be.visible');
    })

    it('В табе "Семинары и конгрессы" есть блок пагинации', () => {
        cy.visit('company/OHI-S/seminars/');

        cy.get('.o-pagination__desktop').should('be.visible');
    })

    it('В табе "Семинары и конгрессы" есть блок скиллов', () => {
        cy.visit('company/OHI-S/seminars/');

        cy.get('.skills-catalog').should('be.visible');
    })

    it('Лекторы: Активный таб Семинары и конгрессы при переходе в Семинары и конгрессы', () => {
        cy.visit('company/OHI-S/lecturers/');

        cy.contains('Braces').click();

        cy.url().should('contain',`/learning/orthodontics/braces-3/`);
    })

    it('Активный таб "Лекторы" при переходе в Онлайн обучение', () => {
        cy.visit('company/OHI-S/');

        cy.get('#tab-3').click().should('have.attr', 'aria-selected', 'true')
    })

    it('В табе "Лекторы" есть блок фильтров', () => {
        cy.visit('company/OHI-S/lecturers/');


        cy.getQa('filter-content').should('be.visible');
    })

    it('В табе "Лекторы" есть блок сортировок', () => {
        cy.visit('company/OHI-S/lecturers/');

        cy.get('.product-catalog__filter-wrapper').should('be.visible');
    })

    it('В табе "Лекторы" есть блок продуктов', () => {
        cy.visit('company/OHI-S/lecturers/');

        cy.getQa('lecturer-card').should('be.visible');
    })

    it('В табе "Лекторы" есть блок пагинации', () => {
        cy.visit('company/OHI-S/lecturers/');

        cy.get('.o-pagination__desktop').should('be.visible');
    })

    it('В табе "Лекторы" есть блок скиллов', () => {
        cy.visit('company/OHI-S/lecturers/');

        cy.get('.skills-catalog').should('be.visible');
    })

    it('Лекторы: Активный таб Лекторы и конгрессы при переходе в Лекторы', () => {
        cy.visit('company/OHI-S/lecturers/');

        cy.contains('Braces').click();

        cy.url().should('contain',`/learning/orthodontics/braces-3/`);
    })

    it('На странице Онлайн-обучения отсутсвует фильтр Организация', () => {
        cy.visit('/company/OHI-S/courses/');

        cy.get('.filter-content').within(() => {
            cy.contains('Organizers').should('not.exist')
        })
    })

    it('На странице Семинары и конгрессы отсутсвует фильтр Организация', () => {
        cy.visit('/company/OHI-S/seminars/');

        cy.get('.filter-content').within(() => {
            cy.contains('Organizers').should('not.exist')
        })
    })

    it('На странице Лекторы отсутсвует фильтр Организация', () => {
        cy.visit('/company/OHI-S/lecturers/');

        cy.get('.filter-content').within(() => {
            cy.contains('Organizers').should('not.exist')
        })
    })

})


