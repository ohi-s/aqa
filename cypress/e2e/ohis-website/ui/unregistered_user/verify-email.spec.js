describe('Verefy-email', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Переход нв главную страницу при нажатии Start learning', () => {
        cy.visit('/verify-email/');

        cy.wait(2000);
        cy.contains('Start learning').click();

        cy.get('.banner-wrapper').should('be.visible');
    })
})

