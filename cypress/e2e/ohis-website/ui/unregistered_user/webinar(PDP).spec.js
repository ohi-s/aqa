describe.skip('Webinar', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Отображается: название веба, Краткое описание, ' +
        'Блок с ценой и трейлером, Урок входит в курс, ' +
        'Поможем с выбором, Программа, Лекторы, ' +
        'Рейтинг, Сертификат, Остались вопросы, Часто задаваемые вопросы, ' +
        'Вам может быть интересно', () => {
        cy.visit('webinars/33592/');

        cy.get('.page-header__title').should('contain', 'Для автотестов 2');//название веба
        cy.get('.page-header__content').should('be.visible');//Краткое описание
        cy.get('.o-content-block__wrapper').should('be.visible');// блок с ценой и трейлером
        cy.get('.product-page-courses__content',{timeout: 5000}).should('contain', 'Цикл для автотестов')//блок Урок входит в курс
        cy.getQa('skills-list').should('contain','Marketing');//навыки
        cy.getQa('lead-form-base').should('be.visible');//Поможем с выбором
        cy.getQa('page-program').should('be.visible');//Программа
        cy.getQa('lecturers-block').should('be.visible');//Лекторы
        //cy.get('.show-rate').should('contain', 'Reviews');//Рейтинг
        cy.get('.certificate-description').should('contain', 'вуцамкпренонгаьтртач');//описание сертификата
        cy.getQa('product-sertificate').should('be.visible');//Сертификат
        cy.getQa('lead-form-base').should('contain', 'Do you have any questions?');//Остались вопросы
        cy.getQa('FAQ-block').should('contain', 'Frequently asked questions');//Часто задаваемые вопросы
    })

    it('Блок с ценой и трейлером: открывается трейлер при нажатии', () => {
        cy.visit('/webinars/33592/');

        cy.get('.o-content-block__wrapper').within(() => {
            cy.get('.product-side-bar__preview').click();
        })

        cy.get('.el-dialog').should('be.visible');
    })

    it('Блок с ценой и трейлером: добавляется в корзину при нажатии на Добавить в корзину', () => {
        cy.visit('/webinars/33592/');

        cy.get('.right-bar__add-to-cart')
            .click();

        cy.get('.el-dialog__header')
            .should('contain', 'Lesson added to cart');
    })

    it('Блок с ценой и трейлером: переход на страницу регистрации при нажатии на Купить сейчас', () => {
        cy.visit('/webinars/33592/');

        cy.get('.right-bar__add-to-cart').click();//delete after add data-test
        cy.get('.cart__pay-button').click();

        cy.url().should('contain',`/registration/`);
    })

    it('Блок Урок входит в курс: отображается скидка', () => {
        cy.visit('/webinars/33592/');

        cy.get('.product-page-courses__subtitle')
            .should('contain', '%')
    })

    it('Блок Урок входит в курс: отображается курс', () => {
        cy.visit('/webinars/33592/');

        cy.get('.product-page-courses')
            .should('be.visible')
    })

    it('Блок Урок входит в курс: переход в курс при нажатии на курс', () => {
        cy.visit('/webinars/33592/');

        cy.getQa('product-card').eq(0).click()

        cy.url().should('contain',`/webinars-courses/`);
    })

    it('Блок Лекторы: Отображается лектор', () => {
        cy.visit('/webinars/33592/');

        cy.get('.lecturer')
            .should('be.visible');
    })

    it.skip('Блок Лекторы: переход на страницу лектора при нажатии на аватарку', () => {
        cy.visit('/webinars/33592/');

        cy.getQa('lecturer-block').within(() => {
            cy.get('.picture').eq(0).click();
        })

        cy.url().should('contain',`/profile/`);
    })

    it('Блок Лекторы: переход на страницу лектора при ФИ', () => {
        cy.visit('/webinars/33592/');

        cy.getQa('lecturer-info').within(() => {
            cy.get('.content__name').eq(0).click();
        })

        cy.url().should('contain',`/profile/`);
    })

    it('Блок Лекторы: у лектора отображается страна', () => {
        cy.visit('/webinars/33592/');

        cy.get('.content__place')
            .should('be.visible')
    })

    it('Блок Лекторы: у лектора отображается рейтинг', () => {
        cy.visit('/webinars/33592/');

        cy.getQa('star-rate')
            .should('be.visible')
    })

    it('Блок Лекторы: описание можно развернуть', () => {
        cy.visit('/webinars/33592/');

        cy.getQa('lecturers-block').within(() => {
            cy.get('.o-collapse-preview__control').within(() => {
                cy.get('.svg-icon')
                    .eq(0)
                    .click();
            })
        })
        cy.getQa('lecturers-block').should('contain','Europe and Asia.')
    })

    it('Блок Сертификата: отображается текст', () => {
        cy.visit('/webinars/33592/');

        cy.get('.product-page-sertificate__title')
            .should('be.visible')
        cy.get('.product-page-sertificate__description')
            .should('be.visible')
    })

    it('Блок Сертификата: отображается изображение', () => {
        cy.visit('/webinars/33592/');

        cy.get('.product-page-sertificate__image')
            .should('be.visible')

    })

    it('Блок Часто задаваемые вопросы: при нажатии раскрывается и отображается текст', () => {
        cy.visit('/webinars/33592/');

        cy.get('.el-collapse-item__header')
            .eq(0)
            .click();

        cy.get('.question__description')
            .eq(0)
            .should('contain', 'The easiest way is to contact our manager and tell us about your doubts. The manager will help you choose the best tariff based on your wishes.')
    })

    it.skip('Блок Вам может быть интересно: открывается курс при нажатии на курс', () => {
        cy.visit('/webinars/33592/');

        cy.get('.adv-image-wrapper')
            .click();

        cy.url().should('contain',`/webinars/`);
    })
})


