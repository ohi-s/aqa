describe('People', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Страница авторизации при нажатии на карточку', () => {
        cy.visit('/social/people/');

        cy.getQa('people-card').eq(0).click();

        cy.url().should('contain', `/login/`);
    })

    it('Страница авторизации при нажатии на кнопку Share contacts', () => {
        cy.visit('/social/people/');

        cy.getQa('people-card_controls').eq(0).click();

        cy.url().should('contain', `/login/`);
    })

    it('Страница профиля при переходе по прямой ссылке на профиль лектора', () => {
        cy.visit('/social/profile/165064/');

        cy.url().should('contain', `/social/profile/165064/`);
        cy.get('.o-typography__heading1').should('contain', 'Gregor Slavicek');
    })
})

