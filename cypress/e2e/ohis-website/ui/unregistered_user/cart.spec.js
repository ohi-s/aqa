describe('Cart', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    context('Действия с корзиной в каталоге', () => {
        it('Открывается поп-ап Урок добавлен в корзину при нажатии в корзину (урок)', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});

            cy.get('[data-test="Lesson added to cart-title"]').should('contain', 'Lesson added to cart')
        })

        it('Поп-ап Урок добавлен в корзину: переход в корзину при нажатии Перейти в корзину', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.contains('Go to cart').click()

            cy.url().should('contain','/cart/');
        })

        it('Поп-ап Урок добавлен в корзину: страница регистрации при нажатии Купить сейчас', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.contains('Buy now').click();

            cy.url().should('contain','/registration/');
        })

        it('Поп-ап Урок добавлен в корзину: открывается весь курс при клике на курс', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.get('.webinar-courses__item').click();

            cy.url().should('contain','/webinars-courses/');
        })
    })

    context('Действия с корзиной в корзине', () => {
        it('Добавленный вебинар можно удалить из корзины', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.visit('/cart/')
            cy.get('.cart-wrapper__body').within(() => {
                cy.get('.trash-button').eq(0).click();
            })

            cy.get('.banner__title').should('contain', 'No products in the cart');
        })

        it('Добавленный цикл можно удалить из корзины', () => {
            cy.visit('/courses/');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.visit('/cart/')
            cy.get('.cart-wrapper__body').within(() => {
                cy.get('.trash-button').eq(0).click();
            })

            cy.get('.banner__title').should('contain', 'No products in the cart');
        })

        it('Добавленный семинар можно удалить из корзины', () => {
            cy.visit('/seminars/');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.visit('/cart/')
            cy.get('.cart-wrapper__body').within(() => {
                cy.get('.trash-button').eq(0).click();
            })

            cy.get('.banner__title').should('contain', 'No products in the cart');
        })

        it('Переход на страницу регистрации при нажатии Перейти к оплате', () => {
            cy.visit('/courses/?page=1&per_page=12&updated_at=desc&product_types=webinar');

            cy.getQa('add-cart-btn').eq(0).click({force: true});
            cy.visit('/cart/')

            cy.get('.el-button--danger').click();

            cy.url().should('contain','/registration/');
        })
    })

    it.only('В пустой корзине отображается Н1 "Your cart is empty" ', () => {
        cy.visit('/cart/')

        cy.getQa('banner__title').should('contain', 'Your cart is empty');
    })

    it.only('Переход в каталог при нажатии на кнопку "Go to catalog" в пустой корзине', () => {
        cy.visit('/cart/')

        cy.get('.banner__link').click();

        cy.url().should('contain','/learning/');
    })

    it.only('Перейти в таб Market', () => {
        cy.visit('/cart/')

        cy.contains('market').click();

        cy.url().should('contain','/cart/?market=true');
    })
})


