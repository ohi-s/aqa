describe.skip('Seminars', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Категории: отображаются курсы по Ортодонтии при выборе Ортодонтия', () => {
        cy.visit('/seminars/');

        cy.get('#tab-orthodontics').click();

        cy.url().should('contain',`/learning/orthodontics/`);
    })

    it('Открывает семинар при нажатии на семинар', () => {
        cy.visit('/seminars/');

        cy.getQa('product-card').eq(0).click();

        cy.url().should('contain','/seminars/');
    })

})


