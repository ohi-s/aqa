describe.skip('Webinar-courses', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Отображается: название курса, Краткое описание, ' +
        'Блок с ценой и трейлером, Урок входит в курс, ' +
        'Поможем с выбором, Программа, Лекторы, ' +
        'Рейтинг, Сертификат, Остались вопросы, Часто задаваемые вопросы, ' +
        'Вам может быть интересно', () => {
        cy.visit('webinars-courses/157653/');

        cy.get('.page-header__title').should('contain', 'Цикл для автотестов');//название цикла
        cy.get('.page-header__content').should('be.visible');//Краткое описание
        cy.get('.o-content-block__wrapper').should('be.visible');// блок с ценой и трейлером
        cy.getQa('skills-list').should('contain','Marketing');//навыки
        cy.get('.trailer-carousel__title', {timeout: 7000}).should('be.visible')//трейлеры
        cy.getQa('page-program').should('contain', 'Training model: "tell-show-do". Detailed video protocols for manufacturing of appliances are waiting for you, which you can return to at any time:')//описание
        cy.getQa('lead-form-base').should('be.visible');//Поможем с выбором
        cy.getQa('page-program').should('contain', 'Для автотестов');//Lesson programs
        cy.getQa('lecturers-block').should('be.visible');//Лекторы
        //cy.get('.show-rate').should('contain', 'Reviews');//Рейтинг
        //cy.get('.product-page__interesting').should('be.visible')//You may be interested
        cy.get('.certificate-description').should('contain', 'Recommended for: Orthodontists, Dental technicians.');//описание сертификата
        cy.getQa('product-sertificate').should('be.visible');//Сертификат
        cy.getQa('lead-form-base').should('contain', 'Do you have any questions?');//Остались вопросы
        cy.getQa('FAQ-block').should('contain', 'Frequently asked questions');//Часто задаваемые вопросы
    })

    it('Краткое описание: отображается рейтинг', () => {
        cy.visit('/webinars-courses/157653/');

        cy.get('.page-header__content').within(() => {
            cy.getQa('star-rate').should('be.visible')
        })
    })

    it('Краткое описание: отображается язык', () => {
        cy.visit('/webinars-courses/157653/');

        cy.get('.page-header__content').within(() => {
            cy.get('.page-header__text-block')
                .should('contain', 'Russian')
        })
    })

    it('Краткое описание: отображается компания', () => {
        cy.visit('/webinars-courses/157653/');

        cy.get('.page-header__content').within(() => {
            cy.get('.page-header__text-block')
                .should('contain', 'OHI-S')
        })
    })

    it.skip('Краткое описание: лектор кликабелен', () => {
        cy.visit('/webinars-courses/157653/');

        cy.get('.page-header__content').within(() => {
            cy.get('.o-avatar__names')
                .click();
        })
    })

    it('Блок с ценой и трейлером: открывается трейлер при нажатии', () => {
        cy.visit('webinars-courses/157653/');

        cy.get('.o-content-block__wrapper').within(() => {
            cy.get('.product-side-bar__preview').click();
        })

        cy.get('.el-dialog__header').should('be.visible');
    })

    //скопировать с зареганного
    it.skip('Блок с ценой и трейлером: добавляется в корзину при нажатии на Добавить в корзину', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('right-bar_add-to-cart')
            .click();

        cy.get('.el-dialog__header')
            .should('contain', 'Lesson added to cart');
    })

    //скопировать с зареганного
    it.skip('Блок с ценой и трейлером: переход на страницу payments при нажатии на Купить сейчас', () => {
        cy.visit('/webinars-courses/157653/');

        cy.contains('Buy now')
            .click();

        cy.url().should('contain',`/registration/`);
    })

    it.skip('Блок с ценой и трейлером: открывается модальное окно  "Добавить в корзину" с выбором уроков при нажатии на выбрать уроки', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('right-bar_actions').within(() => {
            cy.contains('Select lessons')
                .click();
        })

        cy.get('[data-test="Adding to cart-title"]').should('contain', 'Adding to cart')
    })

    it.skip('Блок Урок входит в курс: отображается скидка', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('courses-subtitle')
            .should('contain', '%')
    })

    it('Блок Урок входит в курс: отображается курс', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('product-card')
            .should('be.visible')
    })

    it.skip('Блок Программа: программу можно развернуть', () => {
        cy.visit('/webinars-courses/157653/');

        cy.get('.o-collapse-preview__control')
            .click();
    })

    it.skip('Блок Программа: урок можно добавить в корзину', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('add-cart-btn')
            .should('be.visible')
    })

    it('Блок Лекторы: Отображается лектор', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('lecturer-info')
            .should('be.visible');
    })

    it.skip('Блок Лекторы: переход на страницу лектора при нажатии на аватарку', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('lecturer-avatar')
            .click();

        cy.url().should('contain',`/profile/`);
    })

    it.skip('Блок Лекторы: переход на страницу лектора при ФИ', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('lecturer-info').within(() => {
            cy.getQa('info__content').within(() => {
                cy.get('.content__name')
                    .eq(0)
                    .click();
            })
        })

        cy.url().should('contain',`/profile/`);
    })

    it('Блок Лекторы: у лектора отображается страна', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('content-place')
            .should('be.visible')
    })

    it('Блок Лекторы: у лектора отображается рейтинг', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('star-rate')
            .should('be.visible')
    })

    it.skip('Блок Лекторы: описание можно развернуть', () => {
        cy.visit('/webinars-courses/157653/');

        cy.get('.o-collapse-preview__control')
            .click();
    })

    it('Блок Сертификата: отображается текст', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('product-certificate-title')
            .should('be.visible')
        cy.getQa('product-certificate-description')
            .should('be.visible')
    })

    it('Блок Сертификата: отображается сертификат', () => {
        cy.visit('/webinars-courses/157653/');

        cy.getQa('product-certificate-image')
            .should('be.visible')

    })

    it('Блок Часто задаваемые вопросы: при нажатии раскрывается и отображается текст', () => {
        cy.visit('/webinars-courses/157653/');

        cy.get('.el-collapse-item__header')
            .eq(0)
            .click();

        cy.getQa('question-description')
            .eq(0)
            .should('contain', 'The easiest way is to contact our manager and tell us about your doubts. The manager will help you choose the best tariff based on your wishes.')
    })

    //блок отсутсвует
    // it('Блок Вам может быть интересно: открывается курс при нажатии на курс', () => {
    //     cy.visit('/webinars-courses/157653/');
    //
    //     cy.getQa('adv-image-wrapper')
    //         .click();
    //
    //     cy.url().should('contain',`/webinars/`);
    // })
})








