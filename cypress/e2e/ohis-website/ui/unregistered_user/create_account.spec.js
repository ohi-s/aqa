import {randomString} from "../../../../utils";

describe('Сreate_account', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Переход на страницу Offer при нажатии User Agreement', () => {
        cy.visit('/registration/');

        cy.getQa('user-agreement-link').eq(0).click();

        cy.url().should('include',`/offer/`);
    })

    it('Переход на страницу refund-policy при нажатии Privacy Policy', () => {
        cy.visit('/registration/');

        cy.getQa('user-agreement-link').eq(1).click();

        cy.url().should('include',`/refund-policy/`);
    })

    it('Отображается ошибка This field is required если не ввести Имя', () => {
        cy.visit('/registration/');

        cy.wait(1000);
        cy.getQa('signup-btn').click();

        cy.get('.el-form-item__error').eq(0).should('contain', 'This field is required');
    })

    it('Отображается ошибка This field is required если не ввести Фамилию', () => {
        cy.visit('/registration/');

        cy.wait(1000);
        cy.getQa('signup-btn').click();

        cy.get('.el-form-item__error').eq(1).should('contain', 'This field is required');
    })

    it('Отображается ошибка Entered phone number is incorrect если не ввести номер', () => {
        cy.visit('/registration/');

        cy.wait(1000);
        cy.getQa('signup-btn').click();

        cy.get('.el-form-item__error').eq(2).should('contain', 'Entered phone number is incorrect');
    })

    it('Отображается ошибка This field is required если не ввести емеил', () => {
        cy.visit('/registration/');

        cy.wait(1000);
        cy.getQa('signup-btn').click();

        cy.get('.el-form-item__error').eq(3).should('contain', 'This field is required');
    })

    it('Отображается ошибка The Password field must be at least 8 characters не ввести Пароль', () => {
        cy.visit('/registration/');

        cy.wait(1000);
        cy.getQa('signup-btn').click();

        cy.get('.el-form-item__error').eq(4).should('contain', 'The Password field must be at least 8 characters');
    })

    it('Переход на страницу Log in при нажатии Log in', () => {
        cy.visit('/registration/');

        cy.getQa('login-link').click();

        cy.url().should('include',`/login/`);
    })

    it('Успешная регистрация при вводе всех данных и нажатии Создать аккаунт', () => {
        cy.visit('/registration/')

        cy.wait(8000);
        cy.get('.el-form-item__content ').eq(0).type('Test');
        cy.get('.el-form-item__content ').eq(1).type('Testov');
        cy.get('.el-form-item__content ').eq(2).clear().click().type('+375333123123');
        cy.getQa('email-field').type(randomString());
        cy.get('.el-form-item__content ').eq(4).type('12345678');
        cy.getQa('signup-btn').click();

        cy.url().should('contain', `/`);
        cy.getQa('avatar-btn').should('be.visible')
    })
})


