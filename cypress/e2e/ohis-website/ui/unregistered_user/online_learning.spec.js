describe.skip('Online_learning', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Категории: отображаются курсы по Ортодонтии при выборе Ортодонтия', () => {
        cy.visit('/courses/');

        cy.get('#tab-orthodontics').click();

        cy.url().should('contain',`/learning/orthodontics/`);
    })

    it('Открывает урок при нажатии на урок', () => {
        cy.visit('/courses/?product_types=webinar');

        cy.getQa('product-card').eq(0).click();

        cy.url().should('contain','/webinars/');
    })
})


