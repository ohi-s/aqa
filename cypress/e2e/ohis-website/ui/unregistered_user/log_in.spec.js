describe('Authorization', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.visit('/login?route=%2F')
    })

    it('Успешная авторизация пользователя', () => {
        cy.wait(2000);
        cy.getQa('email-field').type('cypress.test@test.com');
        cy.getQa('password-field').type('password');
        cy.getQa('signIn-btn').click();

        cy.getQa('avatgit checkout OSE-10510-qa-articles-videosar-btn').should('be.visible');
    })

    it('Сообщение The E-mail field must be a valid email при вводе невалидного емеил', () => {
        cy.wait(2000);
        cy.getQa('email-field').type('cypress.test@test.com1');
        cy.getQa('password-field').type('password');
        cy.getQa('signIn-btn').click();

        cy.get('.el-form-item__error').should('contain', 'Entered e-mail is incorrect');
    })

    it('Переход на страницу регистрации при нажатии Create Account', () => {
        cy.getQa('registration-link').click();

        cy.getQa('registration-title').should('contain', 'Create account');
        cy.url().should('include',`/registration/`);
    })

    it('Переход на страницу регистрации восстановления при нажатии Forgot password', () => {
        cy.getQa('forgot-password-link').click();

        cy.getQa('restore-title').should('contain', 'Password restore');
    })

})
