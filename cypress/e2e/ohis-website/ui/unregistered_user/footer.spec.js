describe('Footer', () => {
    it('При нажатии на ссылку "Каталог" переход на страницу Каталог', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('Catalog').click()
        })

        cy.url().should('include',`/learning/`);
    })

    it('При нажатии на ссылку "Статьи и видео" переход на страницу Статьи и видео', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('Articles').click()
        })

        cy.url().should('include',`/articles-videos/`);
    })

    it('При нажатии на ссылку "Стать лектором" переход на страницу Стать лектором', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('Become our lecturer').click()
        })

        cy.url().should('include',`/become-our-lecturer/`);
    })

    it('При нажатии на ссылку "Люди" переход на страницу Люди', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('People').click()
        })

        cy.url().should('include',`/people/`);
    })

    it('При нажатии на ссылку "Компании" переход на страницу Компании', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('Companies').click()
        })

        cy.url().should('include',`/companies/`);
    })

    it('При нажатии на ссылку "Стать партнером" переход на страницу Стать партнером', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('Become our partner').click()
        })

        cy.url().should('include',`/become-our-partner/`);
    })

    it('При нажатии на ссылку "Компании" переход на страницу Компании', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('Companies').click()
        })

        cy.url().should('include', `/companies/`);
    })

    it('При нажатии на ссылку "Сообщения" переход на страницу авторизации', () => {
        cy.visit('/');

        cy.get('.footer-content').within(() => {
            cy.contains('Messages').click()
        })

        cy.url().should('include', `/login/`);
    })

    it('При нажатии на ссылку "Конфиденциальность" переход на страницу Конфиденциальность', () => {
        cy.visit('/');

        cy.getQa('cookie-policy').click();

        cy.url().should('include', `/cookie-policy/`);
    })

    it('При нажатии на ссылку "Политика использования файлов cookie" переход на страницу Политика использования файлов cookie', () => {
        cy.visit('/');

        cy.getQa('cookie-policy').click();

        cy.url().should('include', `/cookie-policy/`);
    })

    it('При нажатии на ссылку "Связаться с нами" переход на страницу Связаться с нами', () => {
        cy.visit('/');

        cy.getQa('contact-us-footer').click();

        cy.url().should('include', `/contacts/`);
    })

    it('При нажатии на ссылку "Способы оплаты" переход на страницу Способы оплаты', () => {
        cy.visit('/');

        cy.getQa('payment-methods-footer').click();

        cy.url().should('include', `/payment-methods/`);
        cy.get('h1').should('contain', 'PAYMENT METHODS');
    })

    it('При нажатии на ссылку "Конфиденциальность" переход на страницу с политикой конфиденциальности', () => {
        cy.visit('/');

        cy.getQa('terms-and-conditions-footer').click();

        cy.url().should('include', `/offer/`);
        cy.get('h1').should('contain', 'Public offer agreement');
    })

    it('Follow the link facebook', () => {
        cy.visit('/');

        cy.getQa('facebook-footer').should('have.attr', 'href', 'https://www.facebook.com/ohisworld');
    })

    it('Follow the link instagram', () => {
        cy.visit('/');

        cy.getQa('instagram-footer').should('have.attr', 'href', 'https://www.instagram.com/dentistry.ohis/');
    })

    it('Follow the link youtube', () => {
        cy.visit('/');

        cy.getQa('youTube-footer').should('have.attr', 'href', 'https://www.youtube.com/channel/UCee-e_2v1DxPCcU1jB3uZ7Q');
    })

    it('Follow the link whatsapp', () => {
        cy.visit('/');

        cy.getQa('whatsapp-footer').should('have.attr', 'href', 'https://wa.me/+37258500595');
    })

    it('Follow the link telegram', () => {
        cy.visit('/');

        cy.getQa('telegram-footer').should('have.attr', 'href', 'https://t.me/ohisworld');
    })

    it.skip('Opening e-mail when clicking "Need help?"', () => {
        cy.visit('/');

        cy.getQa('need-help-footer').click();

        cy.url().should('include', `/chat/`);
    })

    it.skip('При нажатии на кнопку "Google play" открывается Google play', () => {
        cy.visit('/');

        cy.get('.footer-mobile-stores').should('have.attr', 'href', 'https://play.google.com/store/apps/details?id=com.ohi_s.mobile.twa&pli=1');
    })

    it.skip('При нажатии на кнопку "App store" открывается App store', () => {
        cy.visit('/');

        cy.get('.google-play__icon').should('have.attr', 'href', 'https://apps.apple.com/app/id6443446696');
    })

    it('Отображается © 2023 ohi-s.com', () => {
        cy.visit('/');

        cy.get('.footer-copyright').should('contain', '© 2023 ohi-s.com');
    })

    it('EUR отображается по умолчанию', () => {
        cy.visit('/');

        cy.get('.footer').within(() => {
            cy.get('.o-currency-select > .el-dropdown').should('contain', 'EUR');
        })
    })
})

