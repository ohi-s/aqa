describe('Filter', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    context('Общие', () => {
        it('Отобрается блок фильтров', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain', 'Format');



            cy.getQa('filter-content').should('contain', 'Location');

            cy.getQa('filter-content').should('contain', 'Skills');
            cy.getQa('filter-content').should('contain', 'Lecturers');
            cy.getQa('filter-content').should('contain', 'Organizers');
        })

        it('Отображается Location при выборе офлайн', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('Offline learning').click({force: true});
            })

            cy.getQa('filter-content').should('contain', 'Location');
        })

        it('Location не отображается при выборе онлайн', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('Online learning').click({force: true});
            })

            cy.getQa('filter-content').should('contain', 'Location');
        })

        it('Отображается ADA и ADEE при выборе онлайн', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('Online learning').click({force: true});
            })

            cy.getQa('filter-content').should('contain', 'ADA (CE)');
            cy.getQa('filter-content').should('contain', 'ADEE (CPD)');
        })

        it('ADEE не отображается при выборе офлайн', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('Offline learning').click({force: true});
            })

            cy.getQa('filter-content').should('contain', 'ADA (CE)');
            cy.getQa('filter-content').should('not.be.contain', 'ADEE (CPD)');
        })

        it.skip('Фильтр Онлайн обучение + Free + 4,5 и выше + English + ADA + Брекет + Marco + OHI-S', () => {
            cy.visit('/learning/');

            cy.wait(5000)
            cy.getQa('filter-content').within(() => {
                cy.contains('Online learning').click({force: true});
                cy.contains('Free').click({force: true});
                cy.contains('4.5').click({force: true});
                cy.contains('English').click({force: true});
                cy.contains('ADA').click({force: true});
                cy.contains('Braces').click({force: true});
                cy.contains('Marco').click({force: true});
                cy.contains('OHI-S').click({force: true});
            })

            cy.url().should('include',`/free/?languages=en&min_rating=4.5&page=1&product_types=webinar_cycle,webinar&accreditations=ada&skill_slugs=braces-3&organization_ids=1&lecturer_ids=28843&is_free=1`);
        })
    })

    context('Формат', () => {
        it('Отображается фильтр Формат', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain', 'Format');
        })

        it('Формат: Отображается Онлайн обучение, Офлайн обучение, Все', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain','Online learning');
            cy.getQa('filter-content').should('contain','Offline learning');
            cy.getQa('filter-content').should('contain','All');
        })

        it.skip('Формат: Отображаются только онлайн-продуктов при фильтре "Онлайн обучение"', () => {
            cy.visit('/learning/');

            cy.wait(7000)
            cy.contains('Online learning').click({force: true});
            cy.wait(7000)
            cy.get('.products__wrapper').eq(0).click({force: true});

            cy.url().should('/webinars-courses/');
        })

        it.skip('Формат: Отображаются только офлайн-продуктов при фильтре "Оффлайн обучение"', () => {
            cy.visit('/learning/');
            cy.getQa('filter-content').within(() => {
                cy.contains('Offline learning').click();
            })

            cy.url().should('/webinars-courses/');
        })

        it.skip('Формат: Отображается страница learning при нажатии All', () => {
            cy.visit('/learning/');

            cy.url().should('/learning/');
        })

        it.skip('Формат: фильтр сворачивается', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.get('#el-collapse-head-3201').click({force: true});

            cy.contains('Online learning').should('not.be.visible');
        })
    })

    context('Цена', () => {
        it('Отображается фильтр Цена', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain', 'Price');
        })

        it('Цена: отображается Бесплатные, Платные', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain','All');
            cy.getQa('filter-content').should('contain','Free');
        })

        it('Отображаюся бесплатные продукты при выборе Free', () => {
            cy.visit('/learning/');

            cy.wait(10000);
            cy.getQa('filter-content').within(() => {
                cy.contains('Free').click({force: true});
            })

            cy.url().should('include',`/free/`);
        })

        it('Отображаюся кнопка "Watch" при выборе Free', () => {
            cy.visit('/learning/');

            cy.wait(10000);
            cy.getQa('filter-content').within(() => {
                cy.contains('Free').click({force: true});
            })

            cy.getQa('watch-btn').should('be.visible');
        })

        it('Отображаюся платные продукты при выборе All', () => {
            cy.visit('/learning/');

            cy.wait(10000);
            cy.getQa('filter-content').within(() => {
                cy.contains('All').click({force: true});
            })

            cy.get('.products__wrapper').should('contain','€');
            cy.getQa('watch-btn').should('not.exist');
        })

        it.skip('Фильтр цена можно свернуть', () => {
            cy.visit('/courses/');

            cy.getQa('collapse-Price-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()
            })

            cy.contains('Free').should('not.be.visible')
        })
    })

    context('Рейтинг', () => {
        it('Отображается фильтр рейтинг', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain', 'Rating');
        })

        it('Рейтинг: отображается 4.5 и выше, 4 и выше, 3.5 и выше, 3 и выше, Все', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain','4.5 and higher')
            cy.getQa('filter-content').should('contain','4 and higher')
            cy.getQa('filter-content').should('contain','3.5 and higher')
            cy.getQa('filter-content').should('contain','3 and higher')
            cy.getQa('filter-content').should('contain','All')
        })

        it('Отображаются курсы с рейтингом 4+ при выборе 4 и выше', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('4.5 and higher').click({force: true});
            })

            cy.url().should('include',`/learning/?min_rating=4.5`);
        })

        it.skip('Фильтр рейтинг сворачивается', () => {
            cy.visit('/learning/');

            cy.getQa('collapse-Rating-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()
            })

            cy.contains('3 and higher').should('not.be.visible');
        })
    })

    context('Языки', () => {
        it('Отображается фильтр Языки курса', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain', 'Course languages');
        })

        it('Языки: отображается Русский, English, Español, Deutsch, Italiano, Português', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain','Русский')
            cy.getQa('filter-content').should('contain','English')
            cy.getQa('filter-content').should('contain','Español')
            cy.getQa('filter-content').should('contain','Deutsch')
            cy.getQa('filter-content').should('contain','Italiano')
            cy.getQa('filter-content').should('contain','Português')
        })

        it('Языки: Отображаются курсы на английском при выборе English', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('English').click({force: true});
            })

            cy.url().should('contain',`languages=en`);
        })

        it.skip('Языки: Фильтр сворачивается', () => {
            cy.visit('/learning/');

            cy.get('[data-test="collapse-Course languages-filter"]').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()
            })

            cy.contains('English').should('not.be.visible');
        })
    })

    context('Место проведения', () => {
        it('Отображается фильтр Место проведения', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').should('contain', 'Location');
        })

        it('Место проведения: В фильтре отображается поле поиска', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.filter-wrapper__item-box-search').should('be.visible');
            });
        })

        it('Место проведения: отображается список городов', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.o-collapse-preview__content').should('be.visible');
            });
        })

        it('Место проведения: город находится при поиске в фильтре', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.el-input__inner').type('Dubai');

                cy.get('.o-collapse-preview__content').should('contain', 'Dubai');
                cy.get('.o-collapse-preview__content').should('not.contain', 'Moscow');
            });
        })

        it('Место проведения: Отображаются семинары которые будут проходить в Дубае при выборе Дубай', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.get('.el-input__inner').type('Dubai');
                cy.get('.el-checkbox').click();
            });

            cy.url().should('include',`location_ids=6`);
        })

        it('Место проведения: Отображется кнопка "Развернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.getQa('o-collapse-preview__control').should('contain', 'Show more');
            });
        })

        it('Место проведения: список разворачивается при нажатии "Развернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.getQa('o-collapse-preview__control').click();

                cy.get('.o-collapse-preview__content').should('contain', 'Madrid');
            });
        })

        it('Место проведения: Отображется кнопка "Свернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.getQa('o-collapse-preview__control').click();

                cy.getQa('o-collapse-preview__control').should('contain', 'Show less');
            });
        })

        it('Место проведения: Список сворачивается при нажатии "Свернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Location-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.getQa('o-collapse-preview__control').click();
                cy.getQa('o-collapse-preview__control').should('contain', 'Show less');
                cy.getQa('o-collapse-preview__control').click();

                cy.get('.o-collapse-preview__content').should('not.have.text', 'Madrid');
            });
        })

        it.skip('Место проведения: Фильтр сворачивается', () => {
            cy.visit('/learning/');

            cy.get('[data-test="collapse-Course languages-filter"]').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()
            })

            cy.contains('English').should('not.be.visible');
        })

    })

    context('Баллы непрерывного обучения', () => {
        it('Отображается фильтр Баллы непрерывного обучения', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain', 'Continuing Education Сredits');
        })

        it('Баллы: Отображается ADA (CE) и ADEE (CPD', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').should('contain', 'ADA (CE)');
            cy.getQa('filter-content').should('contain', 'ADEE (CPD)');
        })

        it('Баллы: Отображаются ADA (CE) продукты при выборе ADA (CE)', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('ADA (CE)').click({force: true});
            })

            cy.url().should('include',`accreditations=ada`);
        })

        it('Баллы: Баллы: Отображаются ADEE (CPD) продукты при выборе ADEE (CPD)', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('ADEE (CPD)').click({force: true});
            })

            cy.url().should('include',`accreditations=adee`);
        })

        it('Баллы: Отображаются ADA (CE) продукты при выборе ADA (CE)', () => {
            cy.visit('/learning/');

            cy.getQa('filter-content').within(() => {
                cy.contains('ADA (CE)').click({force: true});
                cy.contains('ADEE (CPD)').click({force: true});
            })

            cy.url().should('include',`accreditations=ada,adee`);
        })

        it.skip('Баллы: Фильтр сворачивается', () => {
            cy.visit('/learning/');

            cy.getQa('collapse-Skills-filter').should('be.visible');
        })
    })

    context('Навыки', () => {
        it('Отображается фильтр Навыки', () => {
            cy.visit('/learning/');

            cy.getQa('collapse-Skills-filter').should('contain', 'Skills');
        })

        it('Навыки: В фильтре отображается поле поиска', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Skills-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.filter-wrapper__item-box-search').should('be.visible');
            });
        })

        it('Навыки: отображается список навыков', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Skills-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.o-collapse-preview__content').should('be.visible');
            });
        })

        it('Навыки: Отображается тег навыка при выборе навыка', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Skills-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.get('.el-input__inner').type('Braces');
                cy.get('.el-checkbox').eq(0).click();
            });

            cy.get('.tags__list').should('contain', 'Braces')
        });

        it('Навыки: Отображается продукты категории braces при выбрать braces', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Skills-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.get('.el-input__inner').type('Braces');
                cy.get('.el-checkbox').eq(0).click();
            });

            cy.url().should('include',`skill_slugs=braces-3`);
        })

        it.skip('Навыки: Фильтр можно свернуть', () => {
            cy.visit('/learning/');

            cy.getQa('collapse-Skills-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').dblclick()

                cy.get('.el-input__inner').should('not.be.visible',)
            });
        });
    });

    context('Лекторы', () => {
        it('Отображается фильтр Лекторы', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').should('contain', 'Lecturers');
        })

        it('Лекторы: В фильтре лекторы отображается поле поиска', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.filter-wrapper__item-box-search').should('be.visible');
            });
        })

        it('Лекторы: В фильтре лекторы отображается список лекторов', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.o-collapse-preview__content').should('be.visible');
            });
        })

        it('Лекторы: Лектор находится при поиске в фильтре', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.el-input__inner').type('Rossi');

                cy.get('.o-collapse-preview__content').should('contain', 'Rossi');
                cy.get('.o-collapse-preview__content').should('not.contain', 'Marko');
            });
        })

        it('Лекторы: Отображаются курсы определенного лектора при выборе лектора', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.get('.el-input__inner').type('Rossi');
                cy.get('.el-checkbox').click();
            });

            cy.url().should('include',`lecturer_ids=12177`);
        })

        it('Лекторы: Отображется кнопка "Развернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.getQa('o-collapse-preview__control').should('contain', 'Show more');
            });
        })

        it('Лекторы: список разворачивается при нажатии "Развернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.getQa('o-collapse-preview__control').click();

                cy.get('.o-collapse-preview__content').should('contain', 'Solomonov');
            });
        })

        it('Лекторы: Отображется кнопка "Свернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.getQa('o-collapse-preview__control').click();

                cy.getQa('o-collapse-preview__control').should('contain', 'Show less');
            });
        })

        it('Лекторы: Список сворачивается при нажатии "Свернуть"', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Lecturers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.getQa('o-collapse-preview__control').click();
                cy.getQa('o-collapse-preview__control').should('contain', 'Show less');
                cy.getQa('o-collapse-preview__control').click();

                cy.get('.o-collapse-preview__content').should('not.have.text', 'Rossi');
            });
        })

        it.skip('Лекторы: Фильтр сворачивается', () => {
            cy.visit('/learning/');

            cy.getQa('collapse-Lecturers-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()
            })

            cy.contains('English').should('not.be.visible');
        })

    })

    context('Организации', () => {
        it('Отображается фильтр Организации', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Organizers-filter').should('contain', 'Organizers');
        })

        it('В фильтре Организации отображаются организации', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Organizers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})

                cy.get('.o-collapse-preview__content').should('be.visible');
            });
        })


        it('Отображаются курсы выбранной организации при выборе организации', () => {
            cy.visit('/learning/');

            cy.wait(5000);
            cy.getQa('collapse-Organizers-filter').eq(1).within(() => {
                cy.get('.el-collapse-item__arrow').click({force: true})
                cy.get('.el-input__inner').type('ohi-s');
                cy.get('.el-checkbox').click();
            });

            cy.url().should('include',`organization_ids=1`);
        })

        it.skip('Фильтр организации можно свернуть', () => {
            cy.visit('/learning/');

            cy.getQa('collapse-Organizers-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').dblclick();

                cy.contains('OHI-S').should('not.be.visible',)
            })
        })

    })

    context.skip('Online', () => {
        it('Отображается фильтр Формат', () => {
            cy.visit('/courses/');

            cy.getQa('collapse-Format-filter').should('be.visible')
        })

        it('Отображается фильтр рейтинг', () => {
            cy.visit('/courses/');

            cy.getQa('collapse-Rating-filter').should('be.visible')
        })

        it('Отображается фильтр Цена', () => {
            cy.visit('/courses/');

            cy.getQa('collapse-Price-filter').should('be.visible')
        })

        it('Отображается фильтр Языки курса', () => {
            cy.visit('/courses/');

            cy.get('[data-test="collapse-Course languages-filter"]').should('be.visible')
        })

        it('Отображается фильтр Навыки', () => {
            cy.visit('/courses/');

            cy.getQa('collapse-Skills-filter').should('be.visible');
        })

        it('Отображается фильтр Training format', () => {
            cy.visit('/courses/');

            cy.get('[data-test="collapse-Training format-filter"]').should('be.visible')
        })

        it('Отображается фильтр Лекторы', () => {
            cy.visit('/courses/');

            cy.getQa('collapse-Lecturers-filter').should('be.visible')
        })

        it('Отображается фильтр Организации', () => {
            cy.visit('/courses/');

            cy.getQa('collapse-Organizers-filter').should('be.visible')
        })
    })

    context.skip('Offline', () => {
        it('Отображается фильтр Формат', () => {
            cy.visit('/seminars/');

            cy.getQa('collapse-Format-filter').should('be.visible')
        })

        it('Отображается фильтр рейтинг', () => {
            cy.visit('/seminars/');

            cy.getQa('collapse-Rating-filter').should('be.visible')
        })

        it('Отображается фильтр Цена', () => {
            cy.visit('/seminars/');

            cy.getQa('collapse-Price-filter').should('be.visible')
        })

        it('Отображается фильтр Языки курса', () => {
            cy.visit('/seminars/');

            cy.get('[data-test="collapse-Course languages-filter"]').should('be.visible')
        })

        it('Отображается фильтр Навыки', () => {
            cy.visit('/seminars/');

            cy.getQa('collapse-Skills-filter').should('be.visible');
        })

        it('Отображается фильтр Лекторы', () => {
            cy.visit('/seminars/');

            cy.getQa('collapse-Lecturers-filter').should('be.visible')
        })

        it('Отображается фильтр Организации', () => {
            cy.visit('/seminars/');

            cy.getQa('collapse-Organizers-filter').should('be.visible')
        })
    })

    context.skip('Lecturers', () => {
        it('Отображается фильтр рейтинг', () => {
            cy.visit('/lecturers/');

            cy.getQa('collapse-Rating-filter').should('be.visible')
        })

        it('В фильтре рейтинг отображается 4.5 и выше, 4 и выше, 3.5 и выше, 3 и выше, Все', () => {
            cy.visit('/lecturers/');

            cy.getQa('collapse-Rating-filter').should('contain','4.5 and higher')
            cy.getQa('collapse-Rating-filter').should('contain','4 and higher')
            cy.getQa('collapse-Rating-filter').should('contain','3.5 and higher')
            cy.getQa('collapse-Rating-filter').should('contain','3 and higher')
            cy.getQa('collapse-Rating-filter').should('contain','All')
        })

        it.skip('Отображаются курсы с рейтингом 4.5 и выше при выборе 4.5 и выше', () => {
            cy.visit('/lecturers/');

            cy.getQa('collapse-Rating-filter').should('contain','All')
        })

        it('Фильтр рейтинг сворачивается', () => {
            cy.visit('/lecturers/');

            cy.getQa('collapse-Rating-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()
            })

            cy.contains('3 and higher').should('not.be.visible');
        })

        it('Отображается фильтр Языки курса', () => {
            cy.visit('/lecturers/');

            cy.get('[data-test="collapse-Course languages-filter"]').should('be.visible')
        })

        it('В фильтре языки отображается Русский, English, Español, Deutsch, Italiano', () => {
            cy.visit('/lecturers/');

            cy.get('[data-test="collapse-Course languages-filter"]').within(() => {
                cy.contains('Русский').should('be.visible')
            })
            cy.get('[data-test="collapse-Course languages-filter"]').within(() => {
                cy.contains('English').should('be.visible')
            })
            cy.get('[data-test="collapse-Course languages-filter"]').within(() => {
                cy.contains('Español').should('be.visible')
            })
            cy.get('[data-test="collapse-Course languages-filter"]').within(() => {
                cy.contains('Deutsch').should('be.visible')
            })
            cy.get('[data-test="collapse-Course languages-filter"]').within(() => {
                cy.contains('Italiano').should('be.visible')
            })
        })

        it('Отображаются курсы на английском при выборе English', () => {
            cy.visit('/lecturers/');

            cy.get('[data-test="collapse-Course languages-filter"]').within(() => {
                cy.contains('English').click()
            })

            cy.url().should('contain',`languages=en`);
        })

        it('Фильтр языки сворачивается', () => {
            cy.visit('/lecturers/');

            cy.get('[data-test="collapse-Course languages-filter"]').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()
            })

            cy.contains('English').should('not.be.visible');
        })

        it('Отображается фильтр Организации', () => {
            cy.visit('/lecturers/');

            cy.getQa('collapse-Organizers-filter').should('be.visible')
        })

        it('В фильтре Организации отображаются организации', () => {
            cy.visit('/lecturers/');

            cy.getQa('collapse-Organizers-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').click()

                cy.get('.el-checkbox').should('be.visible',)
            })
        })

        it('Отображаются курсы выбранной организации при выборе организации', () => {
            cy.visit('/lecturers/');

            cy.getQa('collapse-Organizers-filter')
                .eq(0)
                .within(() => {
                    cy.get('.el-collapse-item__header').click()
                cy.contains('OHI-S').click()
            });

            cy.url().should('contain', 'organization_ids=1')
        })

        it('Фильтр организации можно свернуть', () => {
            cy.visit('/learning/');

            cy.getQa('collapse-Organizers-filter').eq(0).within(() => {
                cy.get('.el-collapse-item__header').dblclick();

                cy.contains('OHI-S').should('not.be.visible',)
            })
        })
    })
})


