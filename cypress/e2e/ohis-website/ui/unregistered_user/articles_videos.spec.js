describe('Articles & videos', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Переход в статью при нажатии на статью', () => {
        cy.visit('/articles-videos/');

        cy.getQa('article-card').eq(0).click();

        cy.get('.article').should('be.visible');
    })

    it('Открывается 2 страница при нажатии на 2 в пагинаци', () => {
        cy.visit('/articles-videos/');

        cy.get('.articles-main__pagination-wrapper').within(() => {
            cy.contains('2').click();
        })

        cy.url().should('contain', `/articles-videos/?page=2`);
    })

    it('Открывается 2 страница при нажатии на 2 в пагинаци', () => {
        cy.visit('/articles-videos/?page=2');

        cy.getQa('article-card').eq(0).click();

        cy.get('.article').should('be.visible');
    })

    it('открывается профиль доктора при нажатии на фи в статье', () => {
        cy.visit('/articles-videos/438/');

        cy.getQa('o-avatar').click();

        cy.url().should('contain', `/social/profile/174215/`);
    })

    it('Открывается выбор тарифов подписки при нажатии на Start 7 day free trial', () => {
        cy.visit('/articles-videos/438/');

        cy.wait(2000);
        cy.getQa('banner-slide__link').click();

        cy.getQa('o-title').should('contain', 'Try any subscription tariff for free');
        cy.getQa('subscription-card').should('be.visible');
    })

    it('Открывается страница авторизации при нажатии на Start 7 day free trial в поп-апе выбора тарифов', () => {
        cy.visit('/articles-videos/438/');

        cy.wait(2000);
        cy.getQa('banner-slide__link').click();
        cy.getQa('subscription-card-btn').eq(1).click();

        cy.url().should('contain', `/login/?route=%2Fmy-subscription%2F%3Froute%3D%252Flogin%252F%26freeTrial%3DfreeTrial%26planId%3D14`);
        cy.getQa('login-title').should('contain', 'Log in to start subscription for free');
    })

    it('Отображается блок  Other articles', () => {
        cy.visit('/articles-videos/438/');

        cy.getQa('product-slider').should('contain', `Other articles`);
    })

    it('Отображается блок Other articles', () => {
        cy.visit('/articles-videos/438/');

        cy.getQa('product-slider').should('contain', `Other articles`);
    })

    it('Открывается другая статья при клике на статью в Other articles', () => {
        cy.visit('/articles-videos/438/');

        cy.getQa('product-slider').within(() => {
            cy.get('.articles-slide').eq(0).click();
        })
        cy.url().should('contain', `/455/`);
    })
})

