describe.skip('Search', { tags: '@Unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-13")
    })

    it('Проверить наличие окна для ввода поискового запроса на главной странице', () => {
        cy.visit('/');

        cy.get('.el-input').should('be.visible');
    })

    it('Проверить наличие окна для ввода поискового запроса на странице каталога', () => {
        cy.visit('/learning/');

        cy.get('.el-input').should('be.visible');
    })

    it('Отображается веб при вводе названия веба на русском', () => {
        cy.visit('/');

        cy.get('.el-input').type('Для');
        cy.get('.global-search-product-item').should('contain','Для автотестов 1');
        cy.contains('Для автотестов 1').click();

        cy.url().should('/webinars/33582/');
    })

    it.skip('Отображается веб при вводе названия веба на англ', () => {
        cy.visit('/');

        cy.get('.el-input').type('Для');
        cy.contains('Для автотестов 1').click();

        cy.url().should('/webinars/33582/');
    })

    it.skip('Отображается веб при вводе названия веба на исп', () => {
        cy.visit('/');

        cy.get('.el-input').type('Для');
        cy.contains('Для автотестов 1').click();

        cy.url().should('/webinars/33582/');
    })

    it.skip('Отображается веб при вводе названия веба на нем', () => {
        cy.visit('/');

        cy.get('.el-input').type('Для');
        cy.contains('Для автотестов 1').click();

        cy.url().should('/webinars/33582/');
    })

    it.skip('Отображается веб при вводе названия веба на порт', () => {
        cy.visit('/');

        cy.get('.el-input').type('Для');
        cy.contains('Для автотестов 1').click();

        cy.url().should('/webinars/33582/');
    })

    it.skip('Отображается веб при вводе названия веба на итл', () => {
        cy.visit('/');

        cy.get('.el-input').type('Для');
        cy.contains('Для автотестов 1').click();

        cy.url().should('/webinars/33582/');
    })

    it('Отображается курс при вводе названия курса на русском', () => {
        cy.visit('/');

        cy.get('.el-input').type('Цикл{enter}');
        cy.contains('Цикл для автотестов').click();

        cy.url().should('/webinars-courses/157653/');
    })

    it.skip('Отображается курс при вводе названия курса на англ', () => {
        cy.visit('/');

        cy.get('.el-input').type('Цикл{enter}');
        cy.contains('Цикл для автотестов').click();

        cy.url().should('/webinars-courses/157653/');
    })

    it.skip('Отображается курс при вводе названия курса на исп', () => {
        cy.visit('/');

        cy.get('.el-input').type('Цикл{enter}');
        cy.contains('Цикл для автотестов').click();

        cy.url().should('/webinars-courses/157653/');
    })

    it.skip('Отображается курс при вводе названия курса на нем', () => {
        cy.visit('/');

        cy.get('.el-input').type('Цикл{enter}');
        cy.contains('Цикл для автотестов').click();

        cy.url().should('/webinars-courses/157653/');
    })

    it.skip('Отображается курс при вводе названия курса на порт', () => {
        cy.visit('/');

        cy.get('.el-input').type('Цикл{enter}');
        cy.contains('Цикл для автотестов').click();

        cy.url().should('/webinars-courses/157653/');
    })

    it.skip('Отображается курс при вводе названия курса на итл', () => {
        cy.visit('/');

        cy.get('.el-input').type('Цикл{enter}');
        cy.contains('Цикл для автотестов').click();

        cy.url().should('/webinars-courses/157653/');
    })

    it.skip('Отображается лектор при вводе названия лектор', () => {
        cy.visit('/');

        cy.get('.el-input').type('Лектор');
        cy.contains('Лектор автотест').click();

        cy.url().should('/профиль/номер/');
    })

    it.skip('Проверить поиск по частичному совпадению цифрами', () => {
        cy.visit('/');

        cy.get('.el-input').type('111');
        cy.contains('111').click();

        cy.url().should('/webinars/33582/');
    })

    it('Подгружается страница каталожного вида с результатами поиска, при нажатии на Show all results', () => {
        cy.visit('/');

        cy.get('.el-input').type('Цикл{enter}');

        cy.url().should('/search/');
    })

    it('Переход на вторую страницу на странице поиска', () => {
        cy.visit('/search/online/courses/?search=для');

        cy.get('.number').contains('2').click(CardNumberField)

        cy.url().should('/search/');
    })

    it.skip('Проверить отображение и работу на Android', () => {
        cy.visit('/');
    })

    it.skip('Проверить отображение и работу на iOS', () => {
        cy.visit('/');
    })
})


