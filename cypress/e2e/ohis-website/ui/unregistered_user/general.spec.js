describe('General', { tags: '@unregistered' }, () => {

    // не отображаетя на релизе
    it.skip('Отображается дисконт баннер', () => {
        cy.visit('/');

        cy.get('.discount-banner').should('be.visible');
    })

    it.skip('Баннер не отображается при нажатии на крестик', () => {
        cy.visit('/');

        cy.get('.discount-banner__icon').click()

        cy.get('.discount-banner').should('not.be.visible');
    })

    it('Отображается баннер Куки', () => {
        cy.visit('/');

        cy.get('.o-cookie-banner').should('be.visible');
    })

    it('Отображается главная при нажатии на логотип', () => {
        cy.visit('/login/');

        cy.getQa('logo-btn').click();

        cy.getQa('banner-slide').should('be.visible');
    })

    it('Отображается главная при нажатии на Home', () => {
        cy.visit('/login/');

        cy.get('.header__content').within(() => {
            cy.contains('Home').click();
        })

        cy.getQa('banner-slide').should('be.visible');
    })

    it('Открывается каталог при нажатии Learning', () => {
        cy.visit('/');

        cy.get('.header__content').within(() => {
            cy.contains('Learning').click()
        })

        cy.url().should('contain',`/learning/`);
    })

    it('Открывается страница Люди при нажатии Network', () => {
        cy.visit('/');

        cy.get('.header__content').within(() => {
            cy.contains('Network').click()
        })

        cy.url().should('contain',`/social/people/`);
    })

    it('Открывается страница Заглушки при нажатии Market', () => {
        cy.visit('/');

        cy.get('.header__content').within(() => {
            cy.contains('Market').click()
        })

        cy.url().should('contain',`/market/`);
        //cy.url().should('contain',`/market/not-available/`); заглушка
    })

    it('Отображается поиск', () => {
        cy.visit('/');

        cy.get('.o-global-search-input-wrapper').should('be.visible');
    })

    it('Отображается кнопка Войти', () => {
        cy.visit('/');

        cy.getQa('login-btn').should('be.visible');
    })

    it('Отображается кнопка Создать аккаунт', () => {
        cy.visit('/');

        cy.getQa('registration-btn').should('be.visible');
    })

    it('Переход в авторизацию при нажатии Войти', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.getQa('login-btn').click();

        cy.url().should('contain',`/login/`);
    })

    it('Переход в регистрацию при нажатии Регистрация', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.getQa('registration-btn').click()

        cy.url().should('contain',`/registration/`);
    })

    it('Мои доступы не отображаются', () => {
        cy.visit('/accesses/');

        cy.url().should('contain',`/login/`);
    })

    it('Финансы не отображаются', () => {
        cy.visit('/finance/');

        cy.url().should('contain',`/login/`);
    })

    it('При нажатии на корзину открывается страница корзины', () => {
        cy.visit('/');

        cy.get('.header__right-side').within(() => {
            cy.getQa('el-tooltip').click()
            })

        cy.url().should('contain',`/cart/`);
        cy.getQa('banner__title').should('contain','Your cart is empty');
    })
})