describe('Restore', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it.skip('Сброс пароля при переходе по ссылке из емеила', () => {
    })

    it('Переход на страницу сброса пароля при вводе емейла и нажатии Restore', () => {
        cy.visit('/restore/');

        cy.wait(2000);
        cy.getQa('email-field').type('cypress@test.test');
        cy.wait(2000);
        cy.contains('Restore').click();

        cy.get('.o-typography__paragraph-16-500').should('contain', 'Send again');
    })

    it('Переход на страницу Log in при нажатии Log in', () => {
        cy.visit('/restore/?route=%2Flogin%2F&email=cypress%40test.test');

        cy.wait(1000);
        cy.contains('Log in').click();

        cy.url().should('include',`/login/?route=%2Flogin%2F&email=cypress%40test.test`);
    })

    it('Переход на страницу Create account при нажатии Create account', () => {
        cy.visit('/restore/?route=%2Flogin%2F&email=cypress%40test.test');

        cy.wait(1000);
        cy.contains('Create account').click();

        cy.url().should('include',`/registration/`);
    })

    it('Отображается email на который отправлено смс', () => {
        cy.visit('/restore/?route=%2Flogin%2F&email=cypress%40test.test');

        cy.wait(2000);
        cy.contains('Restore').click();

        cy.get('.restore-to-continue').should('contain', 'cypress@test.test');
    })

    it('Отображается кнопка Send again', () => {
        cy.visit('/restore/?route=%2Flogin%2F&email=cypress%40test.test');

        cy.wait(2000);
        cy.contains('Restore').click();

        cy.get('.o-typography__paragraph-16-500').should('contain', 'Send again');
    })

})


