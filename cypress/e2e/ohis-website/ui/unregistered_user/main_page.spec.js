describe('Main page', { tags: ['@unregistered', 'main_page'] }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Отображается блок главный слайдер', () => {
        cy.visit('/');

        cy.get('.banner-slide').should('be.visible')
    })

    it('Главный слайдер: переход при нажатии перейти в каталог', () => {
        cy.visit('/');

        cy.get('.glide__slide--active > .banner-slide__info').within(() => {
            cy.getQa('banner-slide__link').click();
        })

        cy.url().should('contain',`/learning/`);
    })

    it('Отображается блок категории', () => {
        cy.visit('/');

        cy.get('.main-categories').should('be.visible')
    })

    it('Категории: Переходит в категорию при нажатии на категорию', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.category').eq(0).click();

        cy.url().should('contain',`/courses/orthodontics/`);
    })

    it('Категории: Переход во все категории при нажатии на Все онлайн уроки', () => {
        cy.visit('/');

        cy.get('.category__title').should('contain','Orthodontics')
        cy.contains('All online courses').click();

        cy.url().should('contain',`/courses/`);
    })

    it('Отображается блок Популярные курсы', () => {
        cy.visit('/');

        cy.get('.product-slider').eq(0).should('be.visible')
    })

    it('Популярные курсы: Переходит в курс при нажатии на курс', () => {
        cy.visit('/');

        cy.get('.popular-courses-glider').eq(0).within(() => {
            cy.get('.product-slide__image').eq(0).click()
        });

        cy.url().should('contain',`/webinars-courses/`);
    })

    it('Отображается блок Навыки', () => {
        cy.visit('/');

        cy.get('.skills-wrapper').should('be.visible')
    })

    it('Переходит в навык при нажатии на навык', () => {
        cy.visit('/');

        cy.contains('Braces').click();

        cy.url().should('contain',`/braces-3/`);
    })

    it('Переход в каталог при нажатии смотреть все навыки', () => {
        cy.visit('/');

        cy.contains('See everything in the catalog').click();

        cy.url().should('contain',`/learning/`);
    })

    it('Отображается блок Скидки', () => {
        cy.visit('/');

        cy.get('.product-slider__header').should('contain','Hurry up to buy with discounts')
    })

    it('Скидки: Переходит в курс при нажатии на курс', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.get('.product-slider').eq(1).within(() => {
            cy.get('.product-slide__image').eq(0).click()
        });
    })

    it('Отображается блок Подписка', () => {
        cy.visit('/');

        cy.getQa('subscriptions-block').should('be.visible')
    })

    it('Переход в /subscription/ при нажатии на Узнать больше', () => {
        cy.visit('/');

        cy.get('.subscriptions-title__more').click()

        cy.url().should('contain',`/subscription/`);
    })

    it('Переходи на страницу Войдите, чтобы начать подписку бесплатно при нажатии Войдите для получения', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.getQa('subscription-card-btn').eq(0).click()

        cy.url().should('contain',`/login/`);
    })

    it('Переходи на страницу Войдите, чтобы начать подписку бесплатно при нажатии Начните 7 дней бесплатно', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.getQa('subscription-card-btn').eq(1).click()

        cy.url().should('contain',`/login/`);
    })

    it('При нажатии на "Выбрать план" под одним из планов в блоке Подписка переход на страницу авторизации', () => {
        cy.visit('/');

        cy.wait(2000);
        cy.getQa('subscription-card-btn').eq(1).click()

        cy.url().should('contain',`/login/`);
    })

    it('Отображается блок Отзывы', () => {
        cy.visit('/');

        cy.get('.review-slide').should('be.visible')
    })

    it('Отзывы: переход в профиль при клике на аватарку', () => {
        cy.visit('/');

        cy.get('.review-slide__avatar').eq(0).click();

        cy.url().should('contain',`/profile/`);
    })

    it('Отзывы: переход в профиль при клике на фи', () => {
        cy.visit('/');

        cy.get('.review-slide__label').eq(0).click();

        cy.url().should('contain',`/profile/`);
    })

    it('Отзывы: переход в курс при клике на отзыв', () => {
        cy.visit('/');

        cy.get('.review-slide__text').eq(0).click();

        cy.url().should('contain',`/webinars/`);
    })

    it('Отображается блок Востребованные и популярные лекторы', () => {
        cy.visit('/');

        cy.get('.lecturer-card').should('be.visible')
    })

    it('Востребованные и популярные лекторы: переход в профиль лектора', () => {
        cy.visit('/');

        cy.get('.lecturer-card').eq(0).click();

        cy.url().should('contain',`/profile/`);
    })

    it('Отображается блок Остались вопросы?', () => {
        cy.visit('/');

        cy.get('.lead-form-base').should('be.visible')
    })
})


