describe.skip('Seminar', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Отображается: хедер, название семинара, Краткое описание, ' +
        'Блок с ценой и трейлером, Навыки, ' +
        'Поможем с выбором, Программа, Лекторы, ' +
        'Рейтинг ожидания, Сертификат, Остались вопросы, Часто задаваемые вопросы, ' +
        'Вам может быть интересно', () => {
            cy.visit('/seminar/625/');

            cy.get('.page-header__cover').should('be.visible');//хедер
            cy.get('.page-header__title').should('contain', 'Семинар для автотестов');//название семинара
            cy.get('.page-header__content').should('be.visible');//Краткое описание
            cy.get('.o-content-block__wrapper').should('be.visible');// блок с ценой и заставкой
            cy.getQa('skills-list').should('be.visible');//навыки
            cy.getQa('lead-form-base').should('be.visible');//Поможем с выбором
            cy.getQa('page-program').should('be.visible');//Программа
            cy.getQa('lecturers-block').should('be.visible');//Лекторы
            //cy.get('.expectations-rate').should('contain', 'Expectations rating');//Рейтинг ожидания
            cy.get('.certificate-description').should('contain', 'акуфпероенвочлнпглнплон');//описание Сертифик
            cy.getQa('product-sertificate').should('be.visible');//Сертификат
            cy.getQa('lead-form-base').should('contain', 'Do you have any questions?');//Остались вопросы
            cy.getQa('FAQ-block').should('contain', 'Frequently asked questions');//Часто задаваемые вопросы
    })

    it('Краткое описание: отображается дата проведения', () => {
        cy.visit('/seminar/625/');

        cy.get('.page-header__text-item').should('contain', '08 - 10 Sep')
    })

    it('Краткое описание: отображается язык', () => {
        cy.visit('/seminar/625/');

        cy.get('.page-header__text-item').should('contain', 'Russian')
    })

    it('Краткое описание: отображается компания', () => {
        cy.visit('/seminar/625/');

        cy.get('.page-header__text-item').should('contain', 'OHI-S')
    })

    it.skip('Краткое описание: лектор кликабелен', () => {//оставить 1 лектора
        cy.visit('/seminar/625/');

        cy.getQa('lecture-link').click();

        cy.url().should('contain',`profile/26702/`);
    })

    it('Навыки которым я научусь: отображается навык', () => {
        cy.visit('/seminar/625/');

        cy.getQa('skill-child-item').should('be.visible')
    })

    it('Блок Лекторы: Отображается лектор', () => {
        cy.visit('/seminar/625/');

        cy.getQa('lecturer-info')
            .should('be.visible');
    })

    it.skip('Блок Лекторы: переход на страницу лектора при нажатии на аватарку', () => {
        cy.visit('/seminar/625/');

        cy.getQa('lecturer-block').within(() => {
            cy.get('.picture').eq(0).click();
        })

        cy.url().should('contain',`/profile/`);
    })

    it('Блок Лекторы: переход на страницу лектора при ФИ', () => {
        cy.visit('/seminar/625/');

        cy.getQa('lecturer-info').within(() => {
                cy.get('.content__name').eq(0).click();
        })

        cy.url().should('contain',`/profile/`);
    })

    it('Блок Лекторы: у лектора отображается страна', () => {
        cy.visit('/seminar/625/');

        cy.getQa('lecturers-block').within(() => {
            cy.getQa('content-place')
                .should('be.visible')
        })
    })

    it('Блок Лекторы: у лектора отображается рейтинг', () => {
        cy.visit('/seminar/625/');

        cy.getQa('star-rate')
            .should('be.visible')
    })

    it.skip('Блок Лекторы: описание можно развернуть', () => {
        cy.visit('/seminar/625/');

        cy.getQa('lecturers-block').within(() => {
            cy.get('.o-collapse-preview__control')
                .click();
        })
    })

    it('Блок Сертификата: отображается текст', () => {
        cy.visit('/seminar/625/');

    cy.getQa('product-certificate-title')
            .should('be.visible')
        cy.getQa('product-certificate-description')
            .should('be.visible')
    })

    it('Блок Сертификата: отображается сертификат', () => {
        cy.visit('/seminar/625/');

    cy.getQa('product-certificate-image')
            .should('be.visible')

    })

    it('Блок Часто задаваемые вопросы: при нажатии раскрывается и отображается текст', () => {
        cy.visit('/seminar/625/');

        cy.getQa('FAQ-block').within(() => {
            cy.get('.el-collapse-item__header')
                .eq(0)
                .click();

            cy.get('.question__description')
                .eq(0)
                .should('contain', 'The easiest way is to contact our manager and tell us about your doubts. The manager will help you choose the best tariff based on your wishes.')
        })
    })

    it('Блок с ценой и трейлером:поп-ап Семинар добавлен в корзину при нажатии на Добавить в коризину', () => {
        cy.visit('/seminar/625/');

        cy.get('.right-bar__add-to-cart')
            .click();

        cy.get('.el-dialog__header')
            .should('contain', 'Seminar added to cart');
    })

    it('Блок с ценой и трейлером: переход на страницу registration при нажатии на Купить сейчас', () => {
        cy.visit('/seminar/625/');

        cy.get('.right-bar__add-to-cart').click();//delete after add data-test
        cy.get('.cart__pay-button').click();

        cy.url().should('contain',`/registration/`);
    })

    it('Блок с ценой и заставкой: изменение количества при нажатии на +', () => {
        cy.visit('/seminar/625/');

        cy.getQa('skills-list').should('be.visible')
    })

    it('Блок с ценой и заставкой: изменение количества при нажатии на -', () => {
        cy.visit('/seminar/625/');

        cy.getQa('skills-list').should('be.visible')
    })
})


