describe.skip('Subscription', { tags: '@unregistered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Цена в евро при выборе EUR', () => {
        cy.visit('/subscription');

        cy.getQa('o-switching-language-currency').realHover('mouse');
        cy.getQa('EUR-menu').click({force: true});

        cy.getQa('subscriptions-container').within(() => {
            cy.getQa('price-view').should('contain', '€')
        })
    })

    it('Цена в долларах при выборе USD', () => {
        cy.visit('/subscription');

        cy.getQa('o-switching-language-currency').realHover('mouse');
        cy.getQa('USD-menu').click({force: true});

        cy.getQa('subscriptions-container').within(() => {
            cy.getQa('price-view').should('contain', '$')
        })
    })

    it('Цена в рублях при выборе RUB', () => {
        cy.visit('/subscription');

        cy.getQa('o-switching-language-currency').realHover('mouse');
        cy.getQa('RUB-menu').click({force: true});

        cy.getQa('subscriptions-container').within(() => {
            cy.getQa('price-view').should('contain', '₽')
        })
    })

    it('Открывается страница авторизации при нажатии Выбрать план (помесячная оплата)', () => {
        cy.visit('/subscription');

        cy.get('.subscription-card').eq(0).within(() => {
            cy.get('.el-button').click()
        })

        cy.url().should('contain',`/login/`);
    })

    it('Открывается страница авторизации при нажатии Выбрать план (годовая оплата)', () => {
        cy.visit('/subscription');

        cy.get('.subscriptions').within(() => {
            cy.get('.el-switch__core').click()
        })
        cy.get('.subscription-card').eq(0).within(() => {
            cy.get('.el-button').click()
        })

        cy.url().should('contain',`/login/`);
    })

    it('Стоимсоть меняется при смене на годовую', () => {
        cy.visit('/subscription');

        cy.getQa('subscription-card-btn').eq(0).click()


        cy.url().should('contain',`/login/`);
    })
})


