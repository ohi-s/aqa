describe('Chat', { tags: '@registered' }, () => {
    beforeEach(() => {
        cy.viewport("macbook-15")
    })

    it('Переход нв главную страницу при нажатии Start learning', () => {
        cy.visit('/chat/');

        cy.url().should('contain', `/login/`);
    })
})

