import {paymentStripe} from "../../../utils";
import {registrationUser} from "../../../utils";

for (var i = 1; i < 34; i++) {
    describe('[1]Create_and_payments',function () {
        it('Создача аккаунта + оплата подписки/курса/веба/семинара',function () {
            registrationUser();
            cy.visit('payments/?plan_id=1');
            paymentStripe();
            //оплата подписки
            cy.get('.el-checkbox__inner').click();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            cy.get('.cart-success__order-item').should('contain', 'Tariff plan “2 lessons“');
            //оплата курса
            cy.visit('payments/?route=%2Fwebinars-courses%2F168038%2F&id=168038&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            //оплата веба
            cy.visit('payments/?route=%2Fwebinars%2F167901%2F&id=167901&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            //оплата семинара
            cy.visit('payments/?route=%2Fseminar%2F168112%2F&id=168112&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            cy.get('.cart-success__order-item').should('contain', 'Seminar “Orthodontics in pediatric dentistry: from myth to practice“');
        })
    })
}

for (var j = 1; j < 34; j++) {
    describe('[2]Create_and_payments',function () {
        it('Создача аккаунта + оплата подписки/курса/веба/семинара',function () {
            registrationUser();
            cy.visit('payments/?plan_id=1');
            paymentStripe();
            //оплата подписки
            cy.get('.el-checkbox__inner').click();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            cy.get('.cart-success__order-item').should('contain', 'Tariff plan “2 lessons“');
            //оплата курса
            cy.visit('payments/?route=%2Fwebinars-courses%2F168038%2F&id=168038&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            //оплата веба
            cy.visit('payments/?route=%2Fwebinars%2F167901%2F&id=167901&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            //оплата семинара
            cy.visit('payments/?route=%2Fseminar%2F168112%2F&id=168112&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            cy.get('.cart-success__order-item').should('contain', 'Seminar “Orthodontics in pediatric dentistry: from myth to practice“');
        })
    })
}

for (var y = 1; y < 34; y++) {
    describe('[3]Create_and_payments',function () {
        it('Создача аккаунта + оплата подписки/курса/веба/семинара',function () {
            registrationUser();
            cy.visit('payments/?plan_id=1');
            paymentStripe();
            //оплата подписки
            cy.get('.el-checkbox__inner').click();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            cy.get('.cart-success__order-item').should('contain', 'Tariff plan “2 lessons“');
            //оплата курса
            cy.visit('payments/?route=%2Fwebinars-courses%2F168038%2F&id=168038&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            //оплата веба
            cy.visit('payments/?route=%2Fwebinars%2F167901%2F&id=167901&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            //оплата семинара
            cy.visit('payments/?route=%2Fseminar%2F168112%2F&id=168112&quantity=1&fastBuy=true');
            paymentStripe();
            cy.get('.el-button.education-payments__order-button.el-button--danger').click();
            cy.url().should('contain','/cart/success/');
            cy.get('.o-typography__heading1').should('contain', 'Thank you for your purchase');
            cy.get('.cart-success__order-status').should('contain', 'Payment received');
            cy.get('.cart-success__order-item').should('contain', 'Seminar “Orthodontics in pediatric dentistry: from myth to practice“');
        })
    })
}
